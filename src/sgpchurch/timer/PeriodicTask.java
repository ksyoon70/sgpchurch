package sgpchurch.timer;
 

import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import sgpchurch.board.BoardDBBean;

public class PeriodicTask implements ServletContextListener{
	
	private ScheduledExecutorService scheduler;
	private ServletContext sc;
	private String rootRealPath;
	public void contextInitialized(ServletContextEvent sce){
		
		 this.sc = sce.getServletContext();
		 this.rootRealPath = sc.getRealPath("/");
		 scheduler = Executors.newSingleThreadScheduledExecutor();
		//scheduler.scheduleAtFixedRate(new timerTask(), 0, 15, TimeUnit.MINUTES);
		scheduler.scheduleAtFixedRate(new timerTask(), 0, 120, TimeUnit.SECONDS);
		System.out.println("contextInitialized");	
	}	
	

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		scheduler.shutdownNow();
		System.out.println("contextDestryed");		
	}
	
	class timerTask extends TimerTask
	{
		BoardDBBean dbPro = BoardDBBean.getInstance();
		int count;
		long startTime;  
		public void run(){
			//System.out.println("timerTask");
			try {
				//startTime = System.currentTimeMillis();
				//System.out.println("timerTask:"+ new Timestamp(startTime));
				
				dbPro.delOldArticles("news_board",rootRealPath);
				dbPro.delOldArticles("preach_board",rootRealPath);
				dbPro.delOldArticles("bulletin_board",rootRealPath);
				dbPro.delOldArticles("data_board",rootRealPath);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("bulletin_board count is: " + Integer.toString(count)+ hostName);
		}			
	}
	
}
