package sgpchurch.file;
import static java.net.URLDecoder.decode;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;



public class FileHandleBean{

	private String fileName = null;
	
	public boolean fileDelete(String filePath) throws UnsupportedEncodingException{
			
	
		try{
			this.fileName = decode(filePath,"UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
			e.setStackTrace(null);
		}		
				
		File f = new File(this.fileName);	
		
			
		  if (!f.exists()) {
              System.err.println(fileName + " 존재하지 않습니다!");
              return false;
		  }
		  System.err.println(fileName + " 삭제!");

	    if (f.delete())
	    	return true;
	     else 
	    	return false;
	}
	
	private void decodeURIComponent() {
		// TODO Auto-generated method stub
		
	}

	public boolean chkAccountDir(String account)
	{
		
		boolean returnVal = true;
		
		String path = FileHandleBean.class.getResource("/").getPath();
		//System.out.println(path);
		// change to WEB-INF/Config.xml
		path = path.substring(0,path.length() - 1);
		//System.out.println(path);
		
		path = path.substring(0, path.lastIndexOf("/") + 1);
		//System.out.println(path);
		
		path = path + "config.xml";
		//System.out.println(path);
		
		SAXBuilder builder = new SAXBuilder();
		
		try {
			Document doc = builder.build(path);
			Element root = doc.getRootElement();
			Element baseDir = root.getChild("baseDir");
			//System.out.println(baseDir.getText());
			String imgsDir = baseDir.getText() + "/images/" + account;
			String flashDir = baseDir.getText() + "/flash/" + account;
			String filesDir = baseDir.getText() + "/files/" + account;
			
			
			File imgsD = new File(imgsDir);			
			if(!imgsD.isDirectory())
			{
				if(!imgsD.mkdirs())
				{
					System.err.println("images 디렉토리 생성 실패");
					returnVal = false;
				}
			}			
			File flashD = new File(flashDir);
			if(!flashD.isDirectory())
			{
				if(!flashD.mkdirs())
				{
					System.err.println("flash 디렉토리 생성 실패");
					returnVal = false;
				}
			}
			
			File filesD = new File(filesDir);
			if(!filesD.isDirectory()){
				if(!filesD.mkdirs())
				{
					System.err.println("files 디렉토리 생성 실패");
					returnVal = false;
				}
			}
			
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnVal = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnVal = false;
		}		
		
		return returnVal;
	}
	
	public String getUploadRootDir()
	{		
		
		String path = FileHandleBean.class.getResource("/").getPath();
	
		// change to WEB-INF/Config.xml
		path = path.substring(0,path.length() - 1);	
		
		path = path.substring(0, path.lastIndexOf("/") + 1);
				
		path = path + "config.xml";
				
		SAXBuilder builder = new SAXBuilder();
		
		try {
			Document doc = builder.build(path);
			Element root = doc.getRootElement();
			Element baseDir = root.getChild("baseDir");
			
			return baseDir.getText();			
			
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}		
		
		return "";
	}
			
}
