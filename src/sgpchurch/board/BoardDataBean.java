package sgpchurch.board;

import java.sql.Timestamp;

public class BoardDataBean{

	private int num;
	private int parent;		//���� ���� �θ���� �˷��ش�.
	 private String id;
    private String writer;
    private String subject;
    private String email;
    private String content;
    private String passwd;
    private Timestamp reg_date;    
    private String due_date;
    private int readcount;
    private String ip;
    private int ref;
    private int re_step;	
    private int re_level;
    private String fbuser_id;
    private String fbuser_name;
    private String fbuser_link;
    private String fbuser_pic;    
    private String fbuser_email;

	public void setNum(int num){
    	this.num=num;
    }
	public void setParentNum(int parent){
    	this.parent=parent;
    }
	public void setId (String id) {
        this.id = id;
    }
    public void setWriter (String writer) {
        this.writer = writer;
    }
    public void setSubject (String subject) {
        this.subject = subject;
    }
    public void setEmail (String email) {
        this.email = email;
    }
    public void setContent (String content) {
        this.content = content;
    }
    public void setPasswd (String passwd) {
        this.passwd = passwd;
    }
    public void setReg_date (Timestamp reg_date) {
        this.reg_date = reg_date;
    }
   public void setDue_date (String due_date) {
	   this.due_date = due_date;
	}

	public void setReadcount(int readcount){
	  	this.readcount=readcount;
	}
    public void setIp (String ip) {
        this.ip = ip;
    }
	public void setRef (int ref) {
        this.ref = ref;
    }
	public void setRe_level (int re_level) {
        this.re_level=re_level;
    }
	public void setRe_step (int re_step) {
        this.re_step=re_step;
    }
	public void setFbuser_id (String fbuser_id) {
        this.fbuser_id=fbuser_id;
    }
	public void setFbuser_name (String fbuser_name) {
        this.fbuser_name=fbuser_name;
    }	
	public void setFbuser_link (String fbuser_link) {
        this.fbuser_link=fbuser_link;
    }
	public void setFbuser_pic (String fbuser_pic) {
        this.fbuser_pic=fbuser_pic;
    }
	public void setFbuser_email (String fbuser_email) {
        this.fbuser_email=fbuser_email;
    }
    public int getNum(){
    	return num;
    }
    public int getParentNum(){
    	return parent;
    }
    public String getId () {
        return id;
    }
    public int getReadcount(){
   	    return readcount;
    }
    public String getWriter () {
        return writer;
    }
    public String getSubject () {
        return subject;
    }
    public String getEmail () {
        return email;
    }
    public String getContent () {
        return content;
    }
    public String getPasswd () {
        return passwd;
    }
    public Timestamp getReg_date () {
        return reg_date;
    }
    /*public Timestamp getDue_date () {
        return due_date;
    }*/
    public String getDue_date () {
        return due_date;
    }
    public String getIp () {
        return ip;
    }
    public int getRef () {
        return ref;
    }
	public int getRe_level () {
        return re_level;
    }
	public int getRe_step () {
        return re_step;
    }
	public String getFbuser_id () {
        return fbuser_id;
    }
	public String getFbuser_name () {
        return fbuser_name;
    }	
	public String getFbuser_link() {
        return fbuser_link;
    }
	public String getFbuser_pic() {
        return fbuser_pic;
    }
	public String getFbuser_email () {
        return fbuser_email;
    }
    
}