package sgpchurch.board;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import sgpchurch.file.FileHandleBean;

public class BoardDBBean {
   
	private static BoardDBBean instance = new BoardDBBean();
   
   public static BoardDBBean getInstance() {
       return instance;
   }
   
   private BoardDBBean() {
   }
   
   private Connection getConnection() throws Exception {
     Context initCtx = new InitialContext();
     Context envCtx = (Context) initCtx.lookup("java:comp/env");
     DataSource ds = (DataSource)envCtx.lookup("jdbc/sgpchurchdb");
     return ds.getConnection();
   }

   public void insertArticle(String table, BoardDataBean article) 
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
		ResultSet rs = null;

		int num=article.getNum();
		int ref=article.getRef();
		int re_step=article.getRe_step();
		int re_level=article.getRe_level();
		int number=0;
       String sql="";              
       sql = "select max(num) from " + table;

       try {
           conn = getConnection();

           pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			if (rs.next())
		      number=rs.getInt(1)+1;
		    else
		      number=1; 
		   
		    if (num!=0)   //
		    {  
		      sql="update " + table + " set re_step=re_step+1 where ref= ? and re_step> ?";
             pstmt = conn.prepareStatement(sql);
             pstmt.setInt(1, ref);
			  pstmt.setInt(2, re_step);
			  pstmt.executeUpdate();
			  re_step=re_step+1;
			  re_level=re_level+1;
		     }else{
		  	  ref=number;
			  re_step=0;
			  re_level=0;
		     }	 
           // make query
           sql = "insert into " + table +" (id,writer,email,subject,passwd,reg_date,";
		    sql+="ref,re_step,re_level,content,ip,due_date,fbuser_id,fbuser_name,fbuser_link,fbuser_pic,fbuser_email) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

           pstmt = conn.prepareStatement(sql);
           pstmt.setString(1, article.getId());
           pstmt.setString(2, article.getWriter());
           pstmt.setString(3, article.getEmail());
           pstmt.setString(4, article.getSubject());
           pstmt.setString(5, article.getPasswd());
			pstmt.setTimestamp(6, article.getReg_date());
           pstmt.setInt(7, ref);
           pstmt.setInt(8, re_step);
           pstmt.setInt(9, re_level);
			pstmt.setString(10, article.getContent());
			pstmt.setString(11, article.getIp());
			pstmt.setTimestamp(12, (article.getDue_date().equals("영구") || article.getDue_date() == null) ? null : Timestamp.valueOf(article.getDue_date() + " 00:00:00"));
			pstmt.setString(13, article.getFbuser_id());
			pstmt.setString(14, article.getFbuser_name());
			pstmt.setString(15, article.getFbuser_link());
			pstmt.setString(16, article.getFbuser_pic());
			pstmt.setString(17, article.getFbuser_email());
			
           pstmt.executeUpdate();
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
			if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
   }
   
   public void insertCommentArticle(String table, BoardDataBean article) 
		   throws Exception {
		       Connection conn = null;
		       PreparedStatement pstmt = null;
				ResultSet rs = null;

				int num=article.getNum();
				int ref=article.getRef();
				int re_step=article.getRe_step();
				int re_level=article.getRe_level();
				int number=0;
		       String sql="";              
		       sql = "select max(num) from " + table;

		       try {
		           conn = getConnection();

		           pstmt = conn.prepareStatement(sql);
					rs = pstmt.executeQuery();
					
					if (rs.next())
				      number=rs.getInt(1)+1;
				    else
				      number=1; 
				   
				    if (num!=0)   //
				    {  
				      sql="update " + table + " set re_step=re_step+1 where ref= ? and re_step> ?";
		             pstmt = conn.prepareStatement(sql);
		             pstmt.setInt(1, ref);
					  pstmt.setInt(2, re_step);
					  pstmt.executeUpdate();
					  re_step=re_step+1;
					  re_level=re_level+1;
				     }else{
				  	  ref=number;
					  re_step=0;
					  re_level=0;
				     }	 
		           // make query
		           sql = "insert into " + table +" (parent,id,writer,email,subject,passwd,reg_date,";
				    sql+="ref,re_step,re_level,content,ip,fbuser_id,fbuser_name,fbuser_link,fbuser_pic,fbuser_email) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		           pstmt = conn.prepareStatement(sql);
		           pstmt.setInt(1, article.getNum());
		           pstmt.setString(2, article.getId());
		           pstmt.setString(3, article.getWriter());
		           pstmt.setString(4, article.getEmail());
		           pstmt.setString(5, article.getSubject());
		           pstmt.setString(6, article.getPasswd());
					pstmt.setTimestamp(7, article.getReg_date());
		           pstmt.setInt(8, ref);
		           pstmt.setInt(9, re_step);
		           pstmt.setInt(10, re_level);
					pstmt.setString(11, article.getContent());
					pstmt.setString(12, article.getIp());
					pstmt.setString(13, article.getFbuser_id());
					pstmt.setString(14, article.getFbuser_name());
					pstmt.setString(15, article.getFbuser_link());
					pstmt.setString(16, article.getFbuser_pic());
					pstmt.setString(17, article.getFbuser_email());
					
		           pstmt.executeUpdate();
		       } catch(Exception ex) {
		           ex.printStackTrace();
		       } finally {
					if (rs != null) try { rs.close(); } catch(SQLException ex) {}
		           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
		           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
		       }
		   }
   
	public int getArticleCount(String table)
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
       ResultSet rs = null;
       
       String sql = "select count(*) from " + table + " where parent = 0";

       int x=0;

       try {
           conn = getConnection();
           
           pstmt = conn.prepareStatement(sql);
           rs = pstmt.executeQuery();

           if (rs.next()) {
              x= rs.getInt(1);
			}
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
		return x;
   }
	
	
	public int getArticleCount(String table, String optionString)
			   throws Exception {
			       Connection conn = null;
			       PreparedStatement pstmt = null;
			       ResultSet rs = null;
			       
			       String sql = "select count(*) from " + table + " where parent = 0 " + " and " + optionString;

			       int x=0;

			       try {
			           conn = getConnection();
			           
			           pstmt = conn.prepareStatement(sql);
			           rs = pstmt.executeQuery();

			           if (rs.next()) {
			              x= rs.getInt(1);
						}
			       } catch(Exception ex) {
			           ex.printStackTrace();
			       } finally {
			           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
			           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
			           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
			       }
					return x;
			   }
	
	public int getSubArticleCount(String table, int parent)
			   throws Exception {
			       Connection conn = null;
			       PreparedStatement pstmt = null;
			       ResultSet rs = null;
			       
			       String sql = "select count(*) from " + table + " where parent = ? order by ref asc";

			       int x=0;

			       try {
			           conn = getConnection();
			           
			           pstmt = conn.prepareStatement(sql);
			           pstmt.setInt(1, parent);
			           rs = pstmt.executeQuery();

			           if (rs.next()) {
			              x= rs.getInt(1);
						}
			       } catch(Exception ex) {
			           ex.printStackTrace();
			       } finally {
			           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
			           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
			           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
			       }
					return x;
			   }

	public List getArticles(String table, int start, int end)
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
       ResultSet rs = null;
       String sql = "select * from " + table + " where parent = 0 " + " order by ref desc, re_step asc limit ?,? ";
       List articleList=null;
       try {
           conn = getConnection();
           
           //pstmt = conn.prepareStatement(
           //	"select * from board order by ref desc, re_step asc limit ?,? ");
           pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, start-1);
			pstmt.setInt(2, end);
           rs = pstmt.executeQuery();

           if (rs.next()) {
               articleList = new ArrayList(end);
               do{
                 BoardDataBean article= new BoardDataBean();
				  article.setNum(rs.getInt("num"));
				  article.setParentNum(rs.getInt("parent"));
				  article.setWriter(rs.getString("writer"));
                 article.setEmail(rs.getString("email"));
                 article.setSubject(rs.getString("subject"));
                 article.setPasswd(rs.getString("passwd"));
			      article.setReg_date(rs.getTimestamp("reg_date"));
				  article.setReadcount(rs.getInt("readcount"));
                 article.setRef(rs.getInt("ref"));
                 article.setRe_step(rs.getInt("re_step"));
				  article.setRe_level(rs.getInt("re_level"));
                 article.setContent(rs.getString("content"));
			      article.setIp(rs.getString("ip"));
			       if(rs.getTimestamp("due_date") != null)
				    {
				    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					    article.setDue_date(dateFormat.format(rs.getTimestamp("due_date")));			    	
				    }else
				    {
				    	article.setDue_date("영구");
				    }
			       article.setFbuser_id(rs.getString("fbuser_id"));
			      article.setFbuser_name(rs.getString("fbuser_name"));
			      article.setFbuser_link(rs.getString("fbuser_link"));
			      article.setFbuser_pic(rs.getString("fbuser_pic"));
			      article.setFbuser_email(rs.getString("fbuser_email"));
				  
                 articleList.add(article);
			    }while(rs.next());
			}
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
		return articleList;
   }
	
	public List getArticles(String table, int start, int end, String optionString)
			   throws Exception {
			       Connection conn = null;
			       PreparedStatement pstmt = null;
			       ResultSet rs = null;
			       String sql = "select * from " + table + " where parent = 0 " + " and " + optionString + " order by ref desc, re_step asc limit ?,? ";
			       List articleList=null;
			       try {
			           conn = getConnection();
			           
			           //pstmt = conn.prepareStatement(
			           //	"select * from board order by ref desc, re_step asc limit ?,? ");
			           pstmt = conn.prepareStatement(sql);
			           pstmt.setInt(1, start-1);
						pstmt.setInt(2, end);
			           rs = pstmt.executeQuery();

			           if (rs.next()) {
			               articleList = new ArrayList(end);
			               do{
			                 BoardDataBean article= new BoardDataBean();
							  article.setNum(rs.getInt("num"));
							  article.setParentNum(rs.getInt("parent"));
							  article.setWriter(rs.getString("writer"));
			                 article.setEmail(rs.getString("email"));
			                 article.setSubject(rs.getString("subject"));
			                 article.setPasswd(rs.getString("passwd"));
						      article.setReg_date(rs.getTimestamp("reg_date"));
							  article.setReadcount(rs.getInt("readcount"));
			                 article.setRef(rs.getInt("ref"));
			                 article.setRe_step(rs.getInt("re_step"));
							  article.setRe_level(rs.getInt("re_level"));
			                 article.setContent(rs.getString("content"));
						      article.setIp(rs.getString("ip")); if(rs.getTime("due_date") != null)
							    {
							    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
								    article.setDue_date(dateFormat.format(rs.getTime("due_date")));			    	
							    }else
							    {
							    	article.setDue_date("영구");
							    }
						      article.setFbuser_id(rs.getString("fbuser_id"));
						      article.setFbuser_name(rs.getString("fbuser_name"));
						      article.setFbuser_link(rs.getString("fbuser_link"));
						      article.setFbuser_pic(rs.getString("fbuser_pic"));
						      article.setFbuser_email(rs.getString("fbuser_email"));
							  
			                 articleList.add(article);
						    }while(rs.next());
						}
			       } catch(Exception ex) {
			           ex.printStackTrace();
			       } finally {
			           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
			           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
			           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
			       }
					return articleList;
			   }

   public BoardDataBean getArticle(String table, int num, String session_id)
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
       ResultSet rs = null;
       BoardDataBean article=null;
       String sql="";
       try {
           conn = getConnection();         
           
			sql = "select * from " + table +" where num = ?";
			pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, num);
           rs = pstmt.executeQuery();

           if (rs.next()) {
               article = new BoardDataBean();
                article.setId(rs.getString("id"));
               article.setNum(rs.getInt("num"));
               article.setParentNum(rs.getInt("parent"));
				article.setWriter(rs.getString("writer"));
               article.setEmail(rs.getString("email"));
               article.setSubject(rs.getString("subject"));
               article.setPasswd(rs.getString("passwd"));
			    article.setReg_date(rs.getTimestamp("reg_date"));
				article.setReadcount(rs.getInt("readcount"));
               article.setRef(rs.getInt("ref"));
               article.setRe_step(rs.getInt("re_step"));
				article.setRe_level(rs.getInt("re_level"));
               article.setContent(rs.getString("content"));
			    article.setIp(rs.getString("ip"));
			    if(rs.getTimestamp("due_date") != null)
			    {
			    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				    article.setDue_date(dateFormat.format(rs.getTimestamp("due_date")));			    	
			    }else
			    {
			    	article.setDue_date("영구");
			    }
			   article.setFbuser_id(rs.getString("fbuser_id"));
		      article.setFbuser_name(rs.getString("fbuser_name"));
		      article.setFbuser_link(rs.getString("fbuser_link"));
		      article.setFbuser_pic(rs.getString("fbuser_pic"));
		      article.setFbuser_email(rs.getString("fbuser_email"));
			} 
           //글쓴이와 조회자가 다르면 읽은 조회수를 증가 시킨다.
          if(session_id == null)
           {
        	  sql = "update " + table + " set readcount=readcount+1 where num = ?";
        	  pstmt = conn.prepareStatement(sql);
  			  pstmt.setInt(1, num);
  			  pstmt.executeUpdate();
  			  article.setReadcount(article.getReadcount()+1);  //참조 값을 증가 시켜준다.
           }
          else
          {
        	  if(!session_id.equals(article.getId()))
			  {
        		  sql = "update " + table + " set readcount=readcount+1 where num = ?";
            	  pstmt = conn.prepareStatement(sql);
      			  pstmt.setInt(1, num);
      			  pstmt.executeUpdate();
      			  article.setReadcount(article.getReadcount()+1);  //참조 값을 증가 시켜준다.
        			  
			  }
        	  
          }
			
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
		return article;
   }
   
   public List coGetArticles(String table, int num)
		   throws Exception {
		       Connection conn = null;
		       PreparedStatement pstmt = null;
		       ResultSet rs = null;
		       String sql = "select * from " + table + " where parent = ? order by reg_date asc";
		       List articleList=null;
		       try {
		           conn = getConnection();
		           
		           //pstmt = conn.prepareStatement(
		           //	"select * from board order by ref desc, re_step asc limit ?,? ");
		           pstmt = conn.prepareStatement(sql);
		           pstmt.setInt(1, num);
		           rs = pstmt.executeQuery();

		           if (rs.next()) {
		               articleList = new ArrayList();
		               do{
		                 BoardDataBean article= new BoardDataBean();
						  article.setNum(rs.getInt("num"));
						  article.setParentNum(rs.getInt("parent"));
						  article.setWriter(rs.getString("writer"));
		                 article.setEmail(rs.getString("email"));
		                 article.setSubject(rs.getString("subject"));
		                 article.setPasswd(rs.getString("passwd"));
					      article.setReg_date(rs.getTimestamp("reg_date"));
						  article.setReadcount(rs.getInt("readcount"));
		                 article.setRef(rs.getInt("ref"));
		                 article.setRe_step(rs.getInt("re_step"));
						  article.setRe_level(rs.getInt("re_level"));
		                 article.setContent(rs.getString("content"));
					      article.setIp(rs.getString("ip"));
					      article.setFbuser_id(rs.getString("fbuser_id"));
					      article.setFbuser_name(rs.getString("fbuser_name"));
					      article.setFbuser_link(rs.getString("fbuser_link"));
					      article.setFbuser_pic(rs.getString("fbuser_pic"));
					      article.setFbuser_email(rs.getString("fbuser_email"));
		                 articleList.add(article);
					    }while(rs.next());
					}
		       } catch(Exception ex) {
		           ex.printStackTrace();
		       } finally {
		           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
		           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
		           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
		       }
				return articleList;
		   }
  

   public BoardDataBean updateGetArticle(String table, int num)
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
       ResultSet rs = null;
       BoardDataBean article=null;
       String sql = "select * from " + table + " where num = ?";
       try {
           conn = getConnection();

           //pstmt = conn.prepareStatement(
           //	"select * from board where num = ?");
           
           pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, num);
           rs = pstmt.executeQuery();

           if (rs.next()) {
               article = new BoardDataBean();
               article.setNum(rs.getInt("num"));
				article.setWriter(rs.getString("writer"));
               article.setEmail(rs.getString("email"));
               article.setSubject(rs.getString("subject"));
               article.setPasswd(rs.getString("passwd"));
			    article.setReg_date(rs.getTimestamp("reg_date"));
				article.setReadcount(rs.getInt("readcount"));
               article.setRef(rs.getInt("ref"));
               article.setRe_step(rs.getInt("re_step"));
				article.setRe_level(rs.getInt("re_level"));
               article.setContent(rs.getString("content"));
			    article.setIp(rs.getString("ip"));
			    if(rs.getTimestamp("due_date") != null)
			    {
			    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				    article.setDue_date(dateFormat.format(rs.getTimestamp("due_date")));			    	
			    }else
			    {
			    	article.setDue_date("영구");
			    }
			}
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
		return article;
   }

   public int updateArticle(String table, BoardDataBean article)
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
       ResultSet rs= null;
       
       String dbpasswd="";
       String sql="";
		int x=-1;
       try {
           conn = getConnection();
       
           sql = "select passwd from " + table + " where num = ?";
           pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, article.getNum());
           rs = pstmt.executeQuery();
           
			if(rs.next()){
				
			  dbpasswd= rs.getString("passwd"); 
			  if(dbpasswd.equals(article.getPasswd())){
				 sql="update " + table + " set writer=?,subject=?,passwd=?";
			    sql+=",content=?, due_date=? where num=?";
               pstmt = conn.prepareStatement(sql);

               pstmt.setString(1, article.getWriter());               
               pstmt.setString(2, article.getSubject());
               pstmt.setString(3, article.getPasswd());
               pstmt.setString(4, article.getContent());
               pstmt.setTimestamp(5, (article.getDue_date().equals("영구") || article.getDue_date() == null) ? null : Timestamp.valueOf(article.getDue_date() + " 00:00:00"));
			    pstmt.setInt(6, article.getNum());
               pstmt.executeUpdate();
				x= 1;
			  }else{
				x= 0;
			  }
			}
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
			if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
		return x;
   }
      
   public int deleteArticle(String table, int num, String passwd)
   throws Exception {
       Connection conn = null;
       PreparedStatement pstmt = null;
       ResultSet rs= null;
       String dbpasswd="";
       String sql="";
       int x=-1;
       try {
			conn = getConnection();

			sql = "select passwd from " + table + " where num = ?";
           pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, num);
           rs = pstmt.executeQuery();
           
			if(rs.next()){
				dbpasswd= rs.getString("passwd"); 
				if(dbpasswd.equals(passwd)){
					sql = "delete from " + table + " where num=?";
					pstmt = conn.prepareStatement(sql);
					//pstmt = conn.prepareStatement(
           	   //   "delete from board where num=?");
                   pstmt.setInt(1, num);
                   pstmt.executeUpdate();
					x= 1; //湲�����깃났
				}else
					x= 0; //鍮��踰�� ��┝
			}
			
			
			//remove comments in table.
			
			sql = "select * from " + table + " where parent = ? order by ref asc";
			pstmt = conn.prepareStatement(sql);
	       pstmt.setInt(1, num);
	       rs = pstmt.executeQuery();
	        
	       if(rs.next()){
					sql = "delete from " + table + " where parent = ?";
					pstmt = conn.prepareStatement(sql);				
                  pstmt.setInt(1, num);
                  pstmt.executeUpdate();				
			}
			
       } catch(Exception ex) {
           ex.printStackTrace();
       } finally {
           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
       }
		return x;
   }
   
   public int deleteArticle(String table, int num)
		   throws Exception {
		       Connection conn = null;
		       PreparedStatement pstmt = null;
		       ResultSet rs= null;
		       String dbpasswd="";
		       String sql="";
		       int x=-1;
		       try {
					conn = getConnection();

					sql = "select passwd from " + table + " where num = ?";
		           pstmt = conn.prepareStatement(sql);
		           pstmt.setInt(1, num);
		           rs = pstmt.executeQuery();
		           
					if(rs.next()){
												
						sql = "delete from " + table + " where num=?";
						pstmt = conn.prepareStatement(sql);
						
		              pstmt.setInt(1, num);
		              pstmt.executeUpdate();
						x= 1; //湲�����깃났
						
					}				
					
					//remove comments in table.
					
					sql = "select * from " + table + " where parent = ? order by ref asc";
					pstmt = conn.prepareStatement(sql);
			       pstmt.setInt(1, num);
			       rs = pstmt.executeQuery();
			        
			       if(rs.next()){
							sql = "delete from " + table + " where parent = ?";
							pstmt = conn.prepareStatement(sql);				
		                  pstmt.setInt(1, num);
		                  pstmt.executeUpdate();				
					}
					
		       } catch(Exception ex) {
		           ex.printStackTrace();
		       } finally {
		           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
		           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
		           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
		       }
				return x;
		   }
   
   @SuppressWarnings("deprecation")
public boolean delOldArticles(String table, String rootPath)
		   throws Exception {
		       Connection conn = null;
		       PreparedStatement pstmt = null;
		       ResultSet rs = null;
		       Date dueDate;
		       Date nextDueDate;  //the next day of dueDate 
		       String subject;
		       int num;
		       SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		     
		       
		       Date now = new Date();
		       Calendar cal = Calendar.getInstance();
		       
		       String sql = "select * from " + table + " where due_date IS NOT NULL";


		       try {
		           conn = getConnection();
		           
		           pstmt = conn.prepareStatement(sql);
		           rs = pstmt.executeQuery();

		           while (rs.next()) {
		        	   dueDate = rs.getTimestamp("due_date");
		        	   cal.setTime(dueDate);
		        	   cal.add(Calendar.DATE, 1);
		        	   nextDueDate = cal.getTime();
		        	   num = rs.getInt("num");
		        	   subject = rs.getString("subject");
		        	   if(nextDueDate.before(now))
		        	   {
		        		   // due date is before now
		        		   // remove info
		        		   deleteOldArticle(table,rootPath,num);
		        		   System.out.println(dateFormat.format(nextDueDate));
		        		   System.out.println(table + " " + "번호:" + Integer.toString(num) + " 제목:" + subject + " deleted!" );
		        		}
		        	   
					}
		           //else
		        	   //System.out.println(table + ":" + "has all eternal items");
		       } catch(Exception ex) {
		           ex.printStackTrace();
		       } finally {
		           if (rs != null) try { rs.close(); } catch(SQLException ex) {}
		           if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
		           if (conn != null) try { conn.close(); } catch(SQLException ex) {}
		       }
		       return true;

		   }
   
   private void deleteOldArticle(String tableName,String rootPath, int num) throws Exception
	{
		// Get article number
		
		BoardDataBean article =  getArticle(tableName,num,null);
		
		  int check = deleteArticle(tableName, num);
		  
		  String content = article.getContent();
			String imageUrl = new String("data/upload/images");
			String flashUrl = new String("data/upload/flash");
			String attachUrl = new String("data/upload/files");
			String fileName;
			String subPath;
			String absPath;
			
			// delete image file
			Document doc = Jsoup.parse(content);
			Elements elems = doc.select("img");
			for(Element elem : elems){
				String elemUrl = elem.attr("src");
				if(elemUrl.contains(imageUrl))
				{		
					int index = elemUrl.indexOf(imageUrl);
					subPath = elemUrl.substring(index);
					//absPath = request.getSession().getServletContext().getRealPath("/") + subPath;
					absPath = rootPath + subPath;
							
					FileHandleBean fileHander = new FileHandleBean();
					fileHander.fileDelete(absPath);				
				}
			}
			// delete flash file
			elems = doc.select("embed");
			for(Element elem : elems){
				String elemUrl = elem.attr("src");
				if(elemUrl.contains(flashUrl))
				{			
					int index = elemUrl.indexOf(flashUrl);
					subPath = elemUrl.substring(index);
					//absPath = request.getSession().getServletContext().getRealPath("/") + subPath;
					absPath = rootPath + subPath;
					
					FileHandleBean fileHander = new FileHandleBean();
					fileHander.fileDelete(absPath);				
				}
			}
			//delete attached file
			elems = doc.select("a");
			for(Element elem : elems){
				String elemUrl = elem.attr("href");
				if(elemUrl.contains(attachUrl))
				{			
					int index = elemUrl.indexOf(attachUrl);
					subPath = elemUrl.substring(index);
					//absPath = request.getSession().getServletContext().getRealPath("/") + subPath;
					absPath = rootPath + subPath;
					
					FileHandleBean fileHander = new FileHandleBean();
					fileHander.fileDelete(absPath);				
				}
			}	
		
	}

}

