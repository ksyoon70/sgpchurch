 package sgpchurch.logon;
 
 import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import sgpchurch.encrypt.LocalEncrypter;
 
 public class LogonDBBean {
    
 	private static LogonDBBean instance = new LogonDBBean();
    
    public static LogonDBBean getInstance() {
        return instance;
    }
    
    private LogonDBBean() {
    }
    
    private Connection getConnection() throws Exception {
      Context initCtx = new InitialContext();
      Context envCtx = (Context) initCtx.lookup("java:comp/env");
      DataSource ds = (DataSource)envCtx.lookup("jdbc/sgpchurchdb");
      return ds.getConnection();
    }
 
    public void insertMember(LogonDataBean member) 
    throws Exception {
        Connection conn = null;
        PreparedStatement pstmt = null;
        
        try {
            conn = getConnection();
            
            pstmt = conn.prepareStatement(
            	"insert into member values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            pstmt.setString(1, member.getId());
            pstmt.setString(2, member.getPasswd());
            pstmt.setString(3, member.getName());
            pstmt.setString(4, member.getNickname());
            pstmt.setString(5, member.getEmail());
            pstmt.setString(6, member.getBlog());
            pstmt.setInt(7, member.getRanking());
            pstmt.setTimestamp(8, member.getReg_date());
			 pstmt.setString(9, member.getQuestions());
			 pstmt.setString(10, member.getPasswdhint());
			 pstmt.setString(11, member.getYear());
			 pstmt.setString(12, member.getMonth());
			 pstmt.setString(13, member.getDay());
			 pstmt.setString(14, member.getDuty());
			 
            pstmt.executeUpdate();
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
            if (conn != null) try { conn.close(); } catch(SQLException ex) {}
        }
    }
 
	public int userCheck(String id, String passwd) 
	throws Exception {
		Connection conn = null;
        PreparedStatement pstmt = null;
		ResultSet rs= null;
        String dbpasswd="";
		int x=-1;
		
	    
        
		try {
            conn = getConnection();
            
            pstmt = conn.prepareStatement(
            	"select passwd from member where id = ?");
            pstmt.setString(1, id);
            rs= pstmt.executeQuery();

			if(rs.next()){
				LocalEncrypter encrypt = new LocalEncrypter("MD5", passwd);

				dbpasswd= rs.getString("passwd");
				//??? ?????...
			    if(encrypt.equal(dbpasswd)) 
					x= 1; //password is correct
				else
					x= 0; //password is not correct
			}else
				x= -1;// The id  not exits.
			
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
			if (rs != null) try { rs.close(); } catch(SQLException ex) {}
            if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
            if (conn != null) try { conn.close(); } catch(SQLException ex) {}
        }
		return x;
	}

	public int confirmId(String id) 
	throws Exception {
		Connection conn = null;
        PreparedStatement pstmt = null;
		ResultSet rs= null;
        String dbpasswd="";
		int x=-1;
        
		try {
            conn = getConnection();
            
            pstmt = conn.prepareStatement(
            	"select id from member where id = ?");
            pstmt.setString(1, id);
            rs= pstmt.executeQuery();

			if(rs.next())
				x= 1; //id is exit.
			else
				x= -1;//id is not exit.		
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
			if (rs != null) try { rs.close(); } catch(SQLException ex) {}
            if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
            if (conn != null) try { conn.close(); } catch(SQLException ex) {}
        }
		return x;
	}


    public LogonDataBean getMember(String id)
    throws Exception {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        LogonDataBean member=null;
        try {
            conn = getConnection();
            
            pstmt = conn.prepareStatement(
            	"select * from member where id = ?");
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                member = new LogonDataBean();
                member.setId(rs.getString("id"));
                member.setPasswd(rs.getString("passwd"));
				  member.setName(rs.getString("name"));
				  member.setNickname(rs.getString("nickname"));
			     member.setEmail(rs.getString("email"));
				  member.setBlog(rs.getString("blog"));
				  member.setRanking(rs.getInt("ranking"));
	             member.setReg_date(rs.getTimestamp("reg_date"));
	             member.setQuestions(rs.getString("question"));
	             member.setPasswdhint(rs.getString("passwdhint"));
	             member.setYear(rs.getString("year"));
	             member.setMonth(rs.getString("month"));
	             member.setDay(rs.getString("day"));
	             member.setDuty(rs.getString("duty"));
			}
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) try { rs.close(); } catch(SQLException ex) {}
            if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
            if (conn != null) try { conn.close(); } catch(SQLException ex) {}
        }
		return member;
    }
    
    public void updateMember(LogonDataBean member)
    throws Exception {
        Connection conn = null;
        PreparedStatement pstmt = null;
        
        try {
            conn = getConnection();
            
            pstmt = conn.prepareStatement(
              "update member set passwd=?,name=?,nickname=?,email=?,blog=?,question=?,passwdhint=?,year=?,month=?,day=?,duty=? "+
              "where id=?");
            pstmt.setString(1, member.getPasswd());
            pstmt.setString(2, member.getName());
            pstmt.setString(3, member.getNickname());
            pstmt.setString(4, member.getEmail());
            pstmt.setString(5, member.getBlog());
            pstmt.setString(6, member.getQuestions());
			 pstmt.setString(7, member.getPasswdhint());
			 pstmt.setString(8, member.getYear());
			 pstmt.setString(9, member.getMonth());
			 pstmt.setString(10, member.getDay());
			 pstmt.setString(11, member.getDuty());
            pstmt.setString(12, member.getId());
           
            
            pstmt.executeUpdate();
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
            if (conn != null) try { conn.close(); } catch(SQLException ex) {}
        }
    }
    
    public int deleteMember(String id, String passwd)
    throws Exception {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs= null;
        String dbpasswd="";
        int x=-1;
        try {
			conn = getConnection();

            pstmt = conn.prepareStatement(
            	"select passwd from member where id = ?");
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            
			if(rs.next()){
				dbpasswd= rs.getString("passwd");
				LocalEncrypter encrypt = new LocalEncrypter("MD5", passwd);
				//if(dbpasswd.equals(passwd)){
				if(encrypt.equal(dbpasswd)){
					pstmt = conn.prepareStatement(
            	      "delete from member where id=?");
                    pstmt.setString(1, id);
                    pstmt.executeUpdate();
					x= 1; //password is correct
				}else
					x= 0; // password is incorrect
			}
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) try { rs.close(); } catch(SQLException ex) {}
            if (pstmt != null) try { pstmt.close(); } catch(SQLException ex) {}
            if (conn != null) try { conn.close(); } catch(SQLException ex) {}
        }
		return x;
    }
 }