package sgpchurch.logon;
import java.sql.Timestamp;

public class LogonDataBean{

	private String id;
	private String passwd;
	private String name;
	private String nickname;			//nickname
	private String email;
	private String blog;	
	private int ranking;
	private Timestamp reg_date;
	private String questions;
	private String passwdhint;
	private String year;
	private String month;
	private String day;
	private String duty;

	public void setId (String id){
		this.id = id;
	}
    public void setPasswd (String passwd){
		this.passwd = passwd;
	}
	public void setName (String name){
		this.name = name;
	}
	public void setNickname (String nickname){
		this.nickname = nickname;
	}
	public void setEmail (String email){
		this.email = email;
	}
	public void setBlog (String blog){
		this.blog = blog;
	}
	public void setReg_date (Timestamp reg_date){
		this.reg_date = reg_date;
	}
	public void setRanking (int ranking){
		this.ranking = ranking;
	}
	
	public void setQuestions (String questions){
		this.questions = questions;
	}
	public void setPasswdhint (String passwdhint){
		this.passwdhint = passwdhint;
	}
	public void setYear (String year){
		this.year = year;
	}
	public void setMonth (String month){
		this.month = month;
	}
	public void setDay (String day){
		this.day = day;
	}
	public void setDuty (String duty){
		this.duty = duty;
	}
/*---------------------------------------------------------------------*/
	public String getId(){
 		return id; 
 	}
	public String getPasswd(){
 		return passwd; 
 	}
	public String getName(){
 		return name; 
 	}
	public String getNickname(){
 		return nickname; 
 	}
	public String getEmail(){
 		return email; 
 	}
	public String getBlog(){
 		return blog; 
 	}

	public Timestamp getReg_date(){
 		return reg_date; 
 	}
	public int getRanking(){
 		return ranking; 
 	}
	public String getQuestions(){
		return questions;
	}
	public String getPasswdhint(){
		return passwdhint;
	}
	public String getYear(){
		return year;
	}
	public String getMonth(){
		return month;
	}
	public String getDay(){
		return day;
	}
	public String getDuty(){
		return duty;
	}
}