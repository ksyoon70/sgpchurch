<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="java.io.File"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.net.URL.*"%>

<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "sgpchurch.logon.*" %>

<%!
	String dir;
	String [] filelist;
	java.io.File f;
	static int cur = 0;
	String ptableName = "preach_board";
	String ntableName = "news_board";
	String btableName = "bulletin_board";
	String bannerDir = "upload/banner/";
	int pageSize = 10;
%>

<%
   	String optionString =  request.getParameter("optionString");

	int news_count = 0;
	int bulletin_count = 0;
	int preach_count = 0;
	List particleList = null;
    List narticleList = null;
    List barticleList = null;
    BoardDBBean dbPro = BoardDBBean.getInstance();

    preach_count = dbPro.getArticleCount(ptableName);

    if (preach_count > 0) {
        particleList = dbPro.getArticles(ptableName,1, pageSize);
    }

    news_count = dbPro.getArticleCount(ntableName);

    if (news_count > 0) {
        narticleList = dbPro.getArticles(ntableName,1, pageSize);
    }

 	bulletin_count = dbPro.getArticleCount(btableName);

    if (bulletin_count > 0) {
        barticleList = dbPro.getArticles(btableName,1, pageSize);
    }

%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<link href="style.css?version=190201_10" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<script src="../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<head>
	<title>상계평강교회 홈페이지에 오신 것을 환영합니다.</title>

	<script>
	//초기화시

var fbuser_id = null;
var fbuser_name =  null;
var fbemail = null;
var fbaccessTocken = null;
var fbpicture_path = null;

$(document).ready(function() {
	//alert("readay");
	  $.ajaxSetup({ cache: true });
	  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	FB.Event.subscribe('auth.authResponseChange', function(response) {
	  // add something when facebook user signed up
	  if (response.status === 'connected') {
				//when facebook user is connected
				FB.api('/me', function(user) {
				fbuser_name = user.name;
				fbuser_id = user.id;
				fbLink = user.link;
			});
		}
	});
	});
 });

	var filename = new Array();
	var cur;
	var banner_cur;
	var length = 0;
	var banner_str = new Array();
	banner_str[0] = "예배와...";
	banner_str[1] = "은혜와...";
	banner_str[2] = "감격이 있는...";
	banner_str[3] = "상계평강교회에 오신 것을 환영합니다.";

	if(document.addEventListener)
	{

	document.addEventListener("DOMContentLoaded", function(){
		<%

		String srcDir = null;
		dir = request.getSession().getServletContext().getRealPath("/") + bannerDir;

	   f = new java.io.File(dir);
	   if(f.exists()){
	       filelist = f.list();
	       for(int i=0;i<filelist.length;i++){
	           java.io.File f2 = new java.io.File(dir + "/" + filelist[cur]);
				srcDir = "../" + bannerDir + f2.getName();
				%>

				cur = <%=cur%>;
				filename[cur] = "<%=srcDir%>";
				length = <%=filelist.length%>;
				<%
				cur = (cur + 1) % filelist.length;
	       }
	   }
	%>
	cur = 0;
	banner_cur = 0;
		initImage();
		var timer = setInterval("ontimer()", 5000);


	},false);
	}else
	{
		window.onload = function(){

			<%

			srcDir = null;
			dir = request.getSession().getServletContext().getRealPath("/") + bannerDir;

		   f = new java.io.File(dir);
		   if(f.exists()){
		       filelist = f.list();
		       for(int i=0;i<filelist.length;i++){
		           java.io.File f2 = new java.io.File(dir + "/" + filelist[cur]);
					srcDir = "../" + bannerDir + f2.getName();
					%>

					//System.out.println("cur :" + cur);
					cur = <%=cur%>;
					filename[cur] = "<%=srcDir%>";
					length = <%=filelist.length%>;
					//alert("파일명" + filename[cur] + "cur" + cur);
					<%
					cur = (cur + 1) % filelist.length;
		       }
		   }
		%>

			cur = 0;
			banner_cur = 0;
			initImage();
			var timer = setInterval("ontimer()", 5000);

		}
	}

	function initImage()
	{

		if(!length)
			return;


		// change to removing items to changing style
		cur = (cur + 1) % length;

		for(i = 0;i < length;i++){

			var banner = document.getElementById('banner_pics');
			var img = document.createElement("img");
			img.src = filename[i];
			img.setAttribute("width",banner.clientWidth + "px");
			img.setAttribute("height",banner.clientHeight + "px");
			if(i != cur)
			{
				img.style.display = "none";
			}
			else
			{
				img.style.display = "block";
			}
			banner.appendChild(img);
		}

		//banner text change
		var banner_obj = document.getElementById('banner_btit');
		if(banner_obj)
		{
			banner_obj.innerHTML = banner_str[0];
		}

		return;

	}

	function ontimer()
	{
		if(!length)
			return;

		cur = (cur + 1) % length;

		var banner = document.getElementById('banner_pics');
		var imgs = banner.childNodes;
		for(i = 0;i < imgs.length;i++){

			var img = document.createElement("img")
			img.src = filename[i];
			img.setAttribute("width",banner.clientWidth + "px");
			img.setAttribute("height",banner.clientHeight + "px");
				if(cur == i)
					{
						img.style.display = "block";
					}
				else
					{
						img.style.display = "none";
					}
				banner.replaceChild(img,imgs[i]);
		}


		var banner_obj = document.getElementById('banner_btit');
		banner_cur = (banner_cur + 1) % banner_str.length;
		if(banner_obj)
		{
			banner_obj.innerHTML = banner_str[banner_cur];
		}

		return;

	}
</script>
</head>
<body>
<div style="position:relative; z-index:1; left: -1px ; top: -1px;">
<div id="banner">
<h2 id="banner_btit"></h2>
<div id="banner_pics">
</div>
</div>

<br>
<div style="position:relative; z-index:1; left: -1px ; top: 180px;">
<div id="preach_main">
<h2 class="recent_ptit helv bold">최근 주일 설교</h2>
<ul class="n_text_list">
<%
    if (preach_count == 0) {
%>
<li>최근 설교가 없습니다.</li>
<li></li>
<%}else{

   for (int i = 0 ; i < particleList.size() ; i++) {
      BoardDataBean particle = (BoardDataBean)particleList.get(i);
%>
	 <li><a href="#" onclick="location.href ='../bulletin/content.jsp?num=<%=particle.getNum()%>&pageNum=1&tableName=<%=ptableName%>' + '&id=' + fbuser_id; return false;">
           <%=particle.getSubject()%></a> </li>
<%}
}%>
</ul>
</div>
<div id="service_time">
<h2 class="service_time_btit helv bold">예배 및 모임 안내</h2>
<table class="table">
<tr>
<th>예배</th>
<th>시각</th>
<th>장소</th>
</tr>
<tr>
<td>주일예배</td>
<td>주일오전 11시</td>
<td>예배실</td>
</tr>
<tr>
<td>주일찬양예배</td>
<td>주일오후 2시</td>
<td>예배실</td>
</tr>
<tr>
<td>수요성경공부</td>
<td>수요일저녁 7시30분</td>
<td>예배실</td>
</tr>
<tr>
<td>금요기도회</td>
<td>금요일저녁 9시</td>
<td>예배실</td>
</tr>
<tr>
<td>새벽기도회</td>
<td>매일(월~금)새벽5시30분</td>
<td>예배실</td>
</tr>
<tr>
<td>교회학교</td>
<td>주일오전 9시30분</td>
<td>교육실</td>
</tr>
</table>
</div>
</div>
<div id="news_main">
<h2 class="recent_ntit helv bold">최근 교회 소식</h2>
<ul class="n_text_list">
<%
    if (news_count == 0) {
%>
<li>새로운 소식이 없습니다.</li>
<li></li>
<%}else{

   for (int i = 0 ; i < narticleList.size() ; i++) {
      BoardDataBean narticle = (BoardDataBean)narticleList.get(i);
%>
	 <li><a href="#" onclick="location.href ='../bulletin/content.jsp?num=<%=narticle.getNum()%>&pageNum=1&tableName=<%=ntableName%>' + '&id=' + fbuser_id; return false;">
           <%=narticle.getSubject()%></a> </li>
<%}
}%>
</ul>
</div>
<div id="bulletin_main">
<h2 class="recent_btit helv bold">게시판 소식</h2>
<ul class="n_text_list">
<%
    if (bulletin_count == 0) {
%>
<li>새로운 글이 없습니다.</li>
<li></li>
<%}else{

   for (int i = 0 ; i < barticleList.size() ; i++) {
      BoardDataBean barticle = (BoardDataBean)barticleList.get(i);
%>
	 <li><a href="#" onclick="location.href ='../bulletin/content.jsp?num=<%=barticle.getNum()%>&pageNum=1&tableName=<%=btableName%>' + '&id=' + fbuser_id; return false;">
           <%=barticle.getSubject()%></a> </li>
<%}
}%>
</ul>
</div>
</div>
</body>
</html>
