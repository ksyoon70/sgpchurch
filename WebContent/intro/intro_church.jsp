<!DOCTYPE html>
<%@ page  contentType="text/html; charset=euc-kr"%>
<html>
<head>
<title>상계평강교회소개</title>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=75553f21ca26d4668f5607ba56dc3718"></script>
</head>
<link href="style.css?version=20130907_2" rel="stylesheet" type="text/css">
<body>

<h1 class="intro_btit">1. 교회소개</h1>
<p>
상계평강교회는 1988년3월13일 방영자 전도사님에 의해 창립되었으며, 현재 조수환 목사님께서 맡아 섬기고 계십니다.
</p>

<table>
<tr>
<td class="table_pastor" colspan=2>담임목사:</td>
<td class="table_pastor" colspan=2>조수환</td>
</tr>
<tr>
<td colspan=4></td>
</tr>
<tr>
<tr>
<td class="table_duty">장로:</td>
<td>김도희</td>
<td class="table_duty">반주자:</td>
<td>강은진</td>
</tr>
</table>

<h1 class="intro_btit">2. 교회 위치</h1>
<p>
주소: 139-200 서울특별시 노원구 상계2동 389-426 전화: 02)3391-5767(팩스겸용)
</p>

<h2 class="intro_bsbtit">오시는 길</h2>
<p>
노원역(4호선) 10번 출구 하차후 상계역 방향으로 200m 직진 상계초교차로 건넌 후 좌측 방향으로 70m. 그후 처음만나는 골목으로 들어가서 150m 가량 직진. 중국음식점 락궁에서 좌측방향 20m에 위치
</p>
<center>
	<div id="map" style="width:100%;height:400px;"></div>
<script>
var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
var options = { //지도를 생성할 때 필요한 기본 옵션
	center: new daum.maps.LatLng(37.6592676, 127.0672881), //지도의 중심좌표.
	level: 1 //지도의 레벨(확대, 축소 정도)
};

var map = new daum.maps.Map(container, options); //지도 생성 및 객체 리턴
</script>
<div style="text-align:center; font-weight:bold;	">교회 위치</div>
</center>
	


</body>

</html>
