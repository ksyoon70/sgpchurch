<!DOCTYPE html>

<%@ page contentType = "text/html; charset=utf-8" %>
<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "sgpchurch.logon.*" %>
<%@ page import = "java.sql.Timestamp" %>
<%@ include file="/view/color.jsp"%>
<%@ taglib uri="http://ckfinder.com" prefix="ckfinder"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>

<html lang="ko" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi" />
<link href="common.css?version=20190207_6" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../../jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<!--<script type="text/javascript"src="http://connect.facebook.net/en_US/all.js"></script> -->
<script type="text/javascript" src="../../ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
<script type="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="../../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>




<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi" />
<title>게시판</title>
</head>
<%

   String writer="",email="",passwd="";
   String id = (String)session.getAttribute("memId");
   int num = Integer.parseInt(request.getParameter("num"));
   String pageNum = request.getParameter("pageNum");
   String sessionId = (id != null) ? id  : request.getParameter("id");
   String tableName = request.getParameter("tableName");
   int co_count = 0;		//comment count
   

   SimpleDateFormat sdf = 
        new SimpleDateFormat("yyyy-MM-dd HH:mm");

   try{
      BoardDBBean dbPro = BoardDBBean.getInstance();
      BoardDataBean article =  dbPro.getArticle(tableName,num,sessionId);
  
	  int ref=article.getRef();
	  int re_step=article.getRe_step();
	  int re_level=article.getRe_level();
	  String writer_id = article.getId();
	  boolean isWriter = (writer_id.equals(sessionId)) ? true : false;	  
	  	  	  
	  if(id != null)
	  {
		  LogonDBBean manager = LogonDBBean.getInstance();
		  LogonDataBean c = manager.getMember(id);  
		  email = c.getEmail();
		  writer = c.getName(); 
		  passwd = c.getPasswd();
		  
	  }  
	  
	  //read comment articles
	  List coArticleList = null;
	  co_count = dbPro.getSubArticleCount(tableName,num);
	  if(co_count > 0)
	  {
		  coArticleList =  dbPro.coGetArticles(tableName,num);	  	  
	  }
	  
	  
	
%>
<script type="text/javascript">

var fabceloginChk = false;
var fbuser_id = null;
var fbuser_name =  null;
var fbemail = null;
var fbaccessTocken = null;
var fbpicture_path = null;
  
$(document).ready(function() {
	//alert("test");

$.ajaxSetup({ cache: true });
$.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	FB.Event.subscribe('auth.login', function(response) {
	  // add something when facebook user signed up
	  if (response.status === 'connected') {
				//when facebook user is connected
				FB.api('/me', 'GET',{fields: 'first_name, last_name,name, id, email, picture.width(150).height(150)'}, function(user) {
				fbuser_name = user.name;
				fbuser_id = user.id;
				fbLink = null;
				console.log(user);
				if(fbuser_id != null)
				{
					// if facebook user id is not null,  makes directory
					var url = '../../head/fbloginPro.jsp?fbuser_id='
					url += fbuser_id
					location.href = url;
				}

			});
		}
	},{scope:'public_profile,email'});

	});
    
    $('#thread_list').click(function(e){
        //var msg = 'hahaha'
        //console.log(msg);
        var href = './bulletin/list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>';
	   e.preventDefault();
       $("#main").load(href);
    });
    
  });
	  

var fileurl = "data/upload/files";

	   function showqt(movieurl, wSize, hSize) {
    
	  var detect = navigator.userAgent.toLowerCase();
	  if (detect.indexOf('msie') + 1) {
 
	      var object = document.createElement("span");
	      var output = "";
		    
        output += '<object id="movie" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width= "' + wSize + '" height= "' + hSize + '">';
       	output += '<param name="src" value="' + movieurl + '" >';
		output += '<param name="autoplay" value="true">';
		output += '<param name="pluginspage" value="http://www.apple.com/quicktime/download/">';
		output += '<param name="controller" value="true">';

		object.innerHTML = output;

		return object; 
       
				
	   }
        else {
        var object = document.createElement("object");
		object.setAttribute('height',hSize);
		object.setAttribute('width',wSize);
		object.setAttribute('data',movieurl);
		object.setAttribute('type','video/quicktime');
		object.setAttribute('id','movie');	
						
		var param1 = document.createElement("param");		
		param1.setAttribute('value','http://www.apple.com/quicktime/download/');
		param1.setAttribute('name','pluginurl');
		object.appendChild(param1);
		var param2 = document.createElement("param");		
		param2.setAttribute('value',"true");
		param2.setAttribute('name','controller');
		object.appendChild(param2);
		var param3 = document.createElement("param");		
		param3.setAttribute('value',"true");
		param3.setAttribute('name','autoplay');
		object.appendChild(param3);
		return object;		
	  }


	}

    
function update_article()
{
}
function check_login()
{
    var id = null;
    <%
        id = (String)session.getAttribute("memId");		
    %>
    id = <%=id%>;

    if(id)
    {
        return true;			
    }
    else if(fbuser_id)
    {			
        return true;
    }		
    else
    {

        location.href = "../head/loginForm.jsp";
        return false;

    }
    return true;
}
	
</script>


<body bgcolor="<%=bodyback_c%>">

<table frame="hsides" bordercolor= <%=light_grey%> width="100%" border="1" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="border-collapse:collapse" class="n_text_list"> 
  <tr height="30">
    <td align="left" bgcolor="<%=light_grey%>" >조회수: <%=article.getReadcount()%></td>        
  </tr>
  <tr height="30">
    <td align="left" >글제목: <%=article.getSubject()%></td>    
  </tr>
  <% 
    if(article.getFbuser_name() != null && article.getWriter().matches(article.getFbuser_name())) { %>
	<tr height="50">
    <td align="left" >
     <a href="<%=article.getFbuser_link()%>" target="_blank" ><img src="<%=article.getFbuser_pic()%>" width = "58px" class="comment_image"> </a>
     글쓴이: <span class="fb_name"><a href="<%=article.getFbuser_link()%>" target="_blank" ><%=article.getFbuser_name()%></a></span>, 날짜: <%= sdf.format(article.getReg_date())%>, 종료일:<%=article.getDue_date()%> </td>    
    </tr>
	<% }else{ %>	
    <tr height="30">
    <td align="left" >글쓴이: <%=article.getWriter()%>, 날짜: <%= sdf.format(article.getReg_date())%>, 종료일:<%=article.getDue_date()%> </td>    
    </tr>
    <%}%> 
  <tr style="word-break:break-all;" wrap="hard">
      <!-- article contents -->
   <td class="ckstyle"><%=article.getContent()%></td>
  </tr>
  <tr height="30">
  	 <% if((isWriter) ) {%>
  	    <td bgcolor="<%=light_grey%>" align="left" > 
	  <input type="button" value="글수정" id="modify" onclick="document.location.href='./bulletin/updateForm.jsp?num=<%=article.getNum()%>&pageNum=<%=pageNum%>&tableName=<%=tableName%>&id='+ fbuser_id"> 		 
	  &nbsp;&nbsp;
	  <input type="button" value="글삭제" id="delete" onclick="document.location.href='./bulletin/deleteForm.jsp?num=<%=article.getNum()%>&pageNum=<%=pageNum%>&passwd=<%=article.getPasswd()%>&tableName=<%=tableName%>'">
	   &nbsp;&nbsp;
        <!--
       <input type="button" value="글목록" id="thread_list"
        onclick="document.location.href='./bulletin/list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>'">
        -->
        <input type="button" value="글목록" id="thread_list">
    </td>
  	<%} 
  	else {%>      
    <td bgcolor="<%=light_grey%>" align="left" >    
    
	   &nbsp;&nbsp;
       <input type="button" value="글목록" id="thread_list">       
    </td>
    <%} %>
  </tr>
</table>
<br>

<!-- comment table draw -->
<%
	if(co_count > 0)
	{
			for (int i = 0 ; i < coArticleList.size() ; i++) {
          		BoardDataBean coArticle = (BoardDataBean)coArticleList.get(i);
%>         
<table style="margin-bottom:5px;" cellpadding="0" cellspacing="0" height="1" width="100%">
<tbody><tr>
    <td style="line-height:0;"></td>
    <td style="border-top:1px solid #ddd;" height="1" width="100%"></td>
</tr>
</tbody></table>

<table cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
    <td></td>
    <td style="text-align:left;" valign="top">
		<%
		if(coArticle.getFbuser_id() == null){
		%>	
        <img src="../images/noname.gif" width = "58px" class="comment_image">
        <%}else{ %>
			<a href="<%=coArticle.getFbuser_link()%>" target="_blank" ><img src="<%=coArticle.getFbuser_pic()%>" width = "58px" class="comment_image"> </a>
        <%}%>
    </td>
    <td width="2px"><div style="width:2px;"></div></td>
    <td bgcolor="#dedede" width="2"><div style="width:2px;"></div></td>
    <td><div style="width:10px;"></div></td>

    <td valign="top" width="75%">
        <table cellpadding="0" cellspacing="0" height="28" width="100%" background="../images/co_title_bg.gif">
        <tbody><tr>
            
				<%
				if(coArticle.getFbuser_id() == null){
				%>
				<td width = "60">	
				<span><%=coArticle.getWriter()%></span>
				</td>
				<%}else{ %>
				<td width = "200">	
				<span><a href="<%=coArticle.getFbuser_link()%>" target="_blank" class="fb_name"><%=coArticle.getFbuser_name()%></a></span>
				</td>
				<%}%>	                
            
            <td>                
                <span style="float:left;" ><%= sdf.format(coArticle.getReg_date())%></span>
            </td>
        </tr>
        </tbody>
        </table>
        <table  cellpadding="0" cellspacing="0" width="100%">
        <tbody><tr>
            <td valign="top">
                <div class="ckstyle" id="view_1298660"><%=coArticle.getContent()%></div>
            </td>
        </tr>
        </tbody></table>
        <!-- <div style="text-align:right; padding-right:10px;">
            <span class="mw_basic_comment_reply"><a href="javascript:comment_box('1298660', 'c');">��۾���</a></span>        </div>  -->       
</tr>
<tr>
    <td colspan="4" height="10"></td>
</tr>
</tbody></table>
<%
			}
	}
%>    
<p/>
<table style="margin-bottom:5px;" cellpadding="0" cellspacing="0" height="1" width="87%">
<tbody><tr>
    <td style="line-height:0;"></td>
    <td style="border-top:1px solid #ddd;" height="1" width="100%"></td>
</tr>
</tbody></table>
 
<table cellpadding="0" cellspacing="0" width="90%">
<tbody>
	<tr>
    <td></td>
    <td style="text-align:left;" valign="top">
		<div id="userPic"></div>
        <!--<img src="../images/noname.gif" width = "58px" class="comment_image">-->
    </td>
    <td width="2px"><div style="width:2px;"></div></td>
    <td bgcolor="#dedede" width="2"><div style="width:2px;"></div></td>
    <td><div style="width:10px;"></div></td>
    <td valign="top" width="100%"><div id="userName"></div></td>       
</tr>
</tbody>
</table>

<form method="post" name="writeCommentForm" action="writeComPro.jsp?pageNum=<%=pageNum%>&tableName=<%=tableName%>" onclick="return check_login()">
<input type="hidden" name="num" value="<%=num%>">
<input type="hidden" name="ref" value="<%=ref%>">
<input type="hidden" name="re_step" value="<%=re_step%>">
<input type="hidden" name="re_level" value="<%=re_level%>">
<input type="hidden" name="writer" id="comm_writer" value="<%=writer%>">
<input type="hidden" name="email" value="1234@1234.com">
<input type="hidden" name="passwd" value="1234">
<input type="hidden" name="id" id="comm_id" value="<%=id%>">
<input type="hidden" name="subject" value=" ">
<input type="hidden" name="fbuser_id" id="fbid" value="">
<input type="hidden" name="fbuser_name" id="fbname" value="">
<input type="hidden" name="fbuser_link" id="fblink" value="">
<input type="hidden" name="fbuser_pic" id="fbpic" value="">
<input type="hidden" name="fbuser_email" id="fbemail" value="">
<table frame="hsides" bordercolor= <%=light_grey%> width="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="border-collapse:collapse"> 
  <tr height="30">
      <td align="left" width = '90%'>    
    <textarea class="ckeditor" id="content" name="content"></textarea>
	<script type="text/javascript">
	  
		var editor = CKEDITOR.replace('content',{
			//ckeditor���� custom configuration ������ ����Ϸ��� webroot���� ������Ѵ�.
			customConfig: '../../ckeditor/ckcomment.js'			
		});
		editor.on('instanceReady', function(evt)
		{
			
			
			var focusCnt = 0;			
			//style="color:rgb(169,169,169)" �� ����� ũ�Ұ� IE�� �ν��Ѵ�.
			editor.setData('<p><span style="color:rgb(169,169,169)">여기에 댓글을 입력하세요.</span></p>');	
			editor.on('focus',function(evt){
						
					if(!focusCnt)
					{
							editor.setData("");
							//editor.preventDefault();
					}
					editor.focus();
				focusCnt++;				
			});
			
		});
		
	</script>     
     </td>
     <td valign="bottom" align="center"><button type="submit" style="border-style:none;background-color:#ffffff;"><img src="../images/comment1.jpg" ></button></td>   
  </tr>
  </table>
  </form>
<%
 }catch(Exception e){} 
 %>   
	      
</body>
</html>      
