<!DOCTYPE html>
<%@ page contentType = "text/html; charset=euc-kr" %>
<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "static java.net.URLDecoder.decode" %>
<%@ page import = "sgpchurch.logon.*" %>
<%@ include file="/view/color.jsp"%>
<% request.setCharacterEncoding("euc-kr");%>
<%!
	int pageSize = 10;		//게시판 페이지 싸이즈는 10으로
	String optionString = null;
	String searchText = null;
	String	selectedIndex = null;

	SimpleDateFormat sdf = 
    	new SimpleDateFormat("yyyy-MM-dd HH:mm");
%>

<%
	request.setCharacterEncoding("utf-8");		//한글을 제대로 받으려면 이명령을 써줘야 한다.
	String tableName = request.getParameter("tableName");
    String pageNum = request.getParameter("pageNum");
	optionString =  request.getParameter("optionString");
if(optionString != null){
    	optionString = optionString.replace("%", "%25"); 
		optionString =  decode(optionString,"UTF-8");
    }
	searchText =  request.getParameter("searchText");    
    if(searchText != null)
    {
    	searchText = searchText.replace("%","%25"); 
		searchText =  decode(searchText,"UTF-8");
    }
	selectedIndex =  request.getParameter("selectedIndex");


    if (pageNum == null) {
        pageNum = "1";
    }

    int currentPage = Integer.parseInt(pageNum);
    int startRow = (currentPage - 1) * pageSize + 1;
    int endRow = currentPage * pageSize;
    int count = 0;
    int number=0;

    List articleList = null;
    BoardDBBean dbPro = BoardDBBean.getInstance();
    if(optionString != null)
    	count = dbPro.getArticleCount(tableName,optionString);
    else
    	count = dbPro.getArticleCount(tableName);
    if (count > 0) {
    	if(optionString != null)
        articleList = dbPro.getArticles(tableName,startRow, pageSize,optionString);
    	else
    		articleList = dbPro.getArticles(tableName,startRow, pageSize);
    }

	number=count-(currentPage-1)*pageSize;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<link href="common.css?version=20140108_3" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<script src="../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<head>
<title>게시판</title>
</head>
<script>
var optStr;


var fbuser_id = null;
var fbuser_name =  null;
var fbemail = null;
var fbaccessTocken = null;
var fbpicture_path = null;

$(document).ready(function() {
	//alert("readay");
	  $.ajaxSetup({ cache: true });
	  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	FB.Event.subscribe('auth.authResponseChange', function(response) {
	  // add something when facebook user signed up
	  if (response.status === 'connected') {
				//when facebook user is connected
				FB.api('/me', function(user) {  
				fbuser_name = user.name;				
				fbuser_id = user.id;		
				fbLink = user.link;
				//alert(fbuser_name);				
			});
		}
	});
	});
	
	var stext = document.getElementsByName("search_text")[0];		
	<%
	if(searchText != null)
	{ %>
		stext.value = "<%=searchText%>";
		
	<%		
	}
	%>
	
	var combo = document.getElementsByName("option")[0];
	<%
	if(selectedIndex != null)
	{ %>
		combo.selectedIndex = <%=Integer.parseInt(selectedIndex)%>;		
	<%		
	}
	%>
        
    $("#sub_item ul li a").click(function(e){
    e.preventDefault();
    $("#main").load($(this).attr('href'));
    });
     
    $("#select_link_num a").click(function(e){
    e.preventDefault();
    $("#main").load($(this).attr('href'));
    });
	
  });

function check_login()
{
	var id = null;
	<%
		String id = (String)session.getAttribute("memId");		
	%>
	id = <%=id%>;
	
	if(!id && !fbuser_id)
	{
		alert("홈페이지 로그인을 하거나 Facebook 계정으로 로그인을 하십시오");
		location.href = "../head/loginForm.jsp";
		return false;
	}
	else
	{	
	<%
	LogonDBBean manager = LogonDBBean.getInstance();
	LogonDataBean c = manager.getMember(id);
	%>
	 if(id)
		location.href="writeForm.jsp?&tableName=<%=tableName%>";
	 else if(fbuser_id)
		location.href="writeForm.jsp?&tableName=<%=tableName%>" + '&id=' + fbuser_id;
	
	}
	return false;
}

function search_list()
{
	
    // make optionString
    
    var combo = document.getElementsByName("option")[0];    
    if(document.getElementsByName("search_text")[0].value == "")
	{
	alert("검색어를 입력하십시오.");
	return false;
	}
  
    
    switch(combo.selectedIndex)
    {
    case 0:
    	optStr = "subject like " + escape("'%") +  encodeURIComponent(document.getElementsByName("search_text")[0].value) + escape("%'");    	
    	break;
    case 1:
    	optStr = "subject like " + escape("'%") + encodeURIComponent(document.getElementsByName("search_text")[0].value) + escape("%'") + " or " + " content like " + escape("'%") + encodeURIComponent(document.getElementsByName("search_text")[0].value) + escape("%'");
    	break;
    case 2:
    	optStr = "writer like " + escape("'%") + encodeURIComponent(document.getElementsByName("search_text")[0].value) + escape("%'");
    	break
    }
    var outString = "list.jsp?tableName=<%=tableName%>&optionString=";
    outString += optStr;    //escape() enable special characters like & %
    outString +="&searchText=";
    outString +=encodeURIComponent(document.getElementsByName("search_text")[0].value);
    outString +="&selectedIndex=";
    outString +=combo.selectedIndex;
	location.href = outString;
	return true;
}
     
</script>
<body bgcolor="<%=bodyback_c%>">
<%
String list_string;
if(tableName.equals("news_board"))
	list_string = "교회소식 목록";
else if (tableName.equals("preach_board"))
	list_string = "주일설교 목록";
else if (tableName.equals("bulletin_board"))
	list_string = "게시판 목록";
else if (tableName.equals("data_board"))
	list_string = "자료실 목록";
else
	list_string = "글목록";
%>
<div id="fb-root"></div>

<h2 class="today_tit helv bold"><%=list_string%>(전체 글:<%=count%>)</h2>
<div id="sub_item">
   <ul class="n_text_list">
  <%
    if (count == 0) {
%>
<li>새로운 소식이 없습니다.</li>
<li></li>
<%}else{
  
   for (int i = 0 ; i < articleList.size() ; i++) {
      BoardDataBean article = (BoardDataBean)articleList.get(i);
%>
      <!--
	 <li><a href="#" onclick="location.href ='./bulletin/content.jsp?num=<%=article.getNum()%>&pageNum=1&tableName=<%=tableName%>' + '&id=' + fbuser_id; return false;">
           <%=article.getSubject()%></a> </li>
         -->
      <li><a href='./bulletin/content.jsp?num=<%=article.getNum()%>&pageNum=1&tableName=<%=tableName%>' + '&id=' + fbuser_id'><%=article.getSubject()%></a> </li>
      
<%}
}%>
    </ul>
</div>
<div style="text-align:center" id="select_link_num">
<%
    if (count > 0) {
        int pageCount = count / pageSize + ( count % pageSize == 0 ? 0 : 1);
		 
        int startPage = (int)(currentPage/10)*10+1;
		int pageBlock=10;
        int endPage = startPage + pageBlock-1;
        if (endPage > pageCount) endPage = pageCount;
        
        if (startPage > 10) {    %>
        <a href="./bulletin/list.jsp?tableName=<%=tableName%>&pageNum=<%= startPage - 10 %>">[이전]</a>
<%      }
        for (int i = startPage ; i <= endPage ; i++) {  %>
        <a href="./bulletin/list.jsp?tableName=<%=tableName%>&pageNum=<%= i %>">[<%= i %>]</a>
<%
        }
        if (endPage < pageCount) {  %>
        <a href="./bulletin/list.jsp?tableName=<%=tableName%>&pageNum=<%= startPage + 10 %>">[다음]</a>
<%
        }
    }
%>
</div>
</body>
</html>
