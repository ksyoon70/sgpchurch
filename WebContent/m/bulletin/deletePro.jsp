<!DOCTYPE html>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ page import = "java.sql.Timestamp" %>

<%@ page import = "sgpchurch.file.FileHandleBean"%>
<%@ page import = "org.jsoup.Jsoup" %>
<%@ page import = "org.jsoup.nodes.Document" %> 
<%@ page import = "org.jsoup.nodes.Element" %>
<%@ page import = "org.jsoup.select.Elements" %>

<% request.setCharacterEncoding("euc-kr");%>

<%

// image file delete

	

  String id = (String)session.getAttribute("memId");
  int num = Integer.parseInt(request.getParameter("num"));
  String pageNum = request.getParameter("pageNum");
  String passwd = request.getParameter("passwd");
  String tableName = request.getParameter("tableName");

  BoardDBBean dbPro = BoardDBBean.getInstance();
  BoardDataBean article =  dbPro.getArticle(tableName,num,id);
  int check = dbPro.deleteArticle(tableName, num, passwd);
  
  String content = article.getContent();
	String imageUrl = new String("data/upload/images");
	String flashUrl = new String("data/upload/flash");
	String attachUrl = new String("data/upload/files");
	String fileName;
	String subPath;
	String absPath;
	
	// delete image file
	Document doc = Jsoup.parse(content);
	Elements elems = doc.select("img");
	for(Element elem : elems){
		String elemUrl = elem.attr("src");
		if(elemUrl.contains(imageUrl))
		{
			//int index = elemUrl.lastIndexOf("/");
			//fileName = elemUrl.substring(index+1);
			//absPath = request.getSession().getServletContext().getRealPath("/") + imageUrl + "/" + fileName;

			int index = elemUrl.indexOf(imageUrl);
			subPath = elemUrl.substring(index);
			absPath = request.getSession().getServletContext().getRealPath("/") + subPath;
			
			FileHandleBean fileHander = new FileHandleBean();
			fileHander.fileDelete(absPath);				
		}
	}
	// delete flash file
	elems = doc.select("embed");
	for(Element elem : elems){
		String elemUrl = elem.attr("src");
		if(elemUrl.contains(flashUrl))
		{
			//int index = elemUrl.lastIndexOf("/");
			//fileName = elemUrl.substring(index+1);
			//absPath = request.getSession().getServletContext().getRealPath("/") + attachUrl + "/" + fileName;

			int index = elemUrl.indexOf(flashUrl);
			subPath = elemUrl.substring(index);
			absPath = request.getSession().getServletContext().getRealPath("/") + subPath;
			
			FileHandleBean fileHander = new FileHandleBean();
			fileHander.fileDelete(absPath);				
		}
	}
	//delete attached file
	elems = doc.select("a");
	for(Element elem : elems){
		String elemUrl = elem.attr("href");
		if(elemUrl.contains(attachUrl))
		{
			//int index = elemUrl.lastIndexOf("/");
			//fileName = elemUrl.substring(index+1);
			//absPath = request.getSession().getServletContext().getRealPath("/") + attachUrl + "/" + fileName;
			int index = elemUrl.indexOf(attachUrl);
			subPath = elemUrl.substring(index);
			absPath = request.getSession().getServletContext().getRealPath("/") + subPath;
			
			FileHandleBean fileHander = new FileHandleBean();
			fileHander.fileDelete(absPath);				
		}
	}	
	

  if(check==1){
%>
	  <meta http-equiv="Refresh" content="0;url=list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>" >
<% }else{%>
       <script language="JavaScript">      
       <!--      
         alert("비밀번호가 맞지 않습니다");
         history.go(-1);
       -->
      </script>
<%
    }
 %>
