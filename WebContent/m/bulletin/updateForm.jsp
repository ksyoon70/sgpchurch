<!DOCTYPE html>
<%@ page contentType = "text/html; charset=euc-kr" %>
<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ include file="/view/color.jsp"%>
<%@ taglib uri="http://ckfinder.com" prefix="ckfinder"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<link href="style.css?version=20131220_1" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../ckfinder/ckfinder.js"></script>
<script type="text/javascript"src="http://connect.facebook.net/en_US/all.js"></script>
<script src="../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<head>
<title>게시판</title>

<script language="JavaScript" src="script.js"></script>
</head>

<%	
  String id = (String)session.getAttribute("memId"); 
  String passwd = request.getParameter("passwd");
  int num = Integer.parseInt(request.getParameter("num"));
  String pageNum = request.getParameter("pageNum");
  String tableName = request.getParameter("tableName");
  String idDir;
  try{
      BoardDBBean dbPro = BoardDBBean.getInstance();
      BoardDataBean article =  dbPro.updateGetArticle(tableName,num);
      
      if(id != null)
	  {		
		idDir = id;  
	  }
	  else
	  {
		  // not login
		  idDir = request.getParameter("id");
	  }

%>
<script type="text/javascript">
var fbuser_id = null;
var fbuser_name =  null;
var fbpicture_path = null;
var fbLink = null;

$(document).ready(function() {
	//alert("readay");
		$.ajaxSetup({ cache: true });
	  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	FB.Event.subscribe('auth.authResponseChange', function(response) {
	  // add something when facebook user signed up
	  if (response.status === 'connected') {
				//when facebook user is connected
				FB.api('/me', function(user) {  
				fbuser_name = user.name;				
				fbuser_id = user.id;		
				fbLink = user.link;				
			});
		}
	});
	});
  });

$(function() {
	  $( "#datepicker1" ).datepicker({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    //changeMonth: true, // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
	    //changeYear: true, // 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    dayNames: ['일','월','화','수','목','금','토'],
	    dayNamesShort: ['일','월','화','수','목','금','토'],
	    dayNamesMin: ['일','월','화','수','목','금','토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년',
	    showButtonPanel: true, // 캘린더 하단에 버튼 패널을 표시한다. 
	    closeText: '닫기'  // 닫기 버튼 패널  
	    
	  });	  
	});
	
function reset_content()
{
	var content =  document.getElementById("content");
	CKEDITOR.instances.content.editable().setHtml('');
}

function reset_date()
{
	var date =  document.getElementById("datepicker1");
	date.value="영구";	
}

</script>
<body bgcolor="<%=bodyback_c%>">  
<center><b>글수정</b>
<br>
<form method="post" name="writeform" action="updatePro.jsp?pageNum=<%=pageNum%>&tableName=<%=tableName%>" onsubmit="return writeSave()">
<input type="hidden" name="passwd" value="<%=article.getPasswd()%>">
<input type="hidden" name="num" value="<%=article.getNum()%>">
<input type="hidden" maxlength="10" name="writer" value="<%=article.getWriter()%>">
<table frame="hsides" width="100%" border="1" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="border-collapse:collapse">
  <tr>
    <td align="center" bgcolor="<%=light_grey%>" colspan=2 >유용한 소식을 자유롭게 써주세요.</td>           
  </tr>
  <tr>
    <td  width="70"  bgcolor="<%=light_grey%>" align="center" >글쓴이</td>    
    <td align="left" >
    <%=article.getWriter()%>   
    </td>       
  </tr>
  <tr>
    <td  width="70"  bgcolor="<%=light_grey%>" align="center" >제 목</td>    
    <td align="left" width="330" >
    <input  class="basic_write_title" type="text"  maxlength="50" name="subject" value="<%=article.getSubject()%>" ></td>              
  </tr>
  <tr>
    <td  width="70"  bgcolor="<%=light_grey%>" align="center" >게시종료일</td>
    <td  width="330" noWrap>    <!-- noWrap 줄바꿈 금지 --> 
         <input type="button" value="영구적용" OnClick="reset_date()"><input class="basic_datepicker" type="text" name="due_date" value="<%=article.getDue_date()%>" id="datepicker1">      
     </td>		
  </tr>   
  <tr>
    <td  width="70"  bgcolor="<%=light_grey%>" align="center" >내 용</td>
    <td align="left">    
    <textarea class="ckeditor" id="content" name="content" ><%=article.getContent()%></textarea>
	<script type="text/javascript">
		var editor = CKEDITOR.replace('content',{
			//ckeditor에서 custom configuration 파일을 사용하려면 webroot까지 써줘야한다.
			customConfig: '/ckeditor/ckupdate.js',
			width:'100%',
			height:300,
			filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : 
		 	   '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files&currentFolder=/<%=id%>/',
			filebrowserImageUploadUrl : 
			   '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images&currentFolder=/<%=id%>/',
			filebrowserFlashUploadUrl : '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash&currentFolder=/<%=id%>/',	
			
		});
			
	</script>     
     </td>
  </tr>  
  <tr>      
   <td colspan=2 bgcolor="<%=light_grey%>" align="center"> 
     <input type="submit" value="글수정" >  
     <!--  <input type="reset" value="다시작성"> -->
     <input type="button" value="내용초기화" onclick="reset_content()">
     <input type="button" value="목록보기" 
       onclick="document.location.href='list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>'">
   </td>
 </tr>
 </table>
</form>
<%
}catch(Exception e){}%>      
      
</body>
</html>      
