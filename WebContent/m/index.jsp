<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="java.io.File"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.net.URL.*"%>

<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "sgpchurch.logon.*" %>

<%! 
	String dir;
	String [] filelist;
	java.io.File f;
	static int cur = 0;
	String ntableName = "news_board";
	String btableName = "bulletin_board";
	String bannerDir = "upload/banner/";
	int pageSize = 10;
%>
<%
   	String optionString =  request.getParameter("optionString");

	int news_count = 0;
	int bulletin_count = 0;
    List narticleList = null;
    List barticleList = null;
    BoardDBBean dbPro = BoardDBBean.getInstance();
    
    news_count = dbPro.getArticleCount(ntableName);   
    
    if (news_count > 0) {    	
        narticleList = dbPro.getArticles(ntableName,1, pageSize);    	
    }
    
 	bulletin_count = dbPro.getArticleCount(btableName);   
    
    if (bulletin_count > 0) {    	
        barticleList = dbPro.getArticles(btableName,1, pageSize);    	
    }
	
%>
<html lang="ko">
<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi" />
 <title>상계평강교회 홈페이지에 오신것을 환영합니다.</title>
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://uhb.kr/gil/ms/002/app_icon.png" />
 <link rel="apple-touch-icon-precomposed" href="http://uhb.kr/gil/ms/002/app_icon.png" />
    <!--
 <script type="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
-->
<script src="../../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
 <script type="text/javascript">
  // <![CDATA[
  var fbuser_id = null;
var fbuser_name =  null;
var fbemail = null;
var fbaccessTocken = null;
var fbpicture_path = null;

$(document).ready(function() {

	  $.ajaxSetup({ cache: true });
	  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	FB.Event.subscribe('auth.authResponseChange', function(response) {
	  // add something when facebook user signed up
	  if (response.status === 'connected') {
				//when facebook user is connected
				FB.api('/me', function(user) {  
				fbuser_name = user.name;				
				fbuser_id = user.id;		
				fbLink = user.link;				
			});
		}
	});
	});
    /* Jquery를 추가 시키면 이상하게 찾기 버튼이 동작하지 않는다. Document Ready와 관련 있는 듯 하다.*/
    $("#btn1").toggle(function() {
    $("#main_head_content").css("display",'block');
    }, function() {
    $("#main_head_content").css("display","none");
    });
    
      
    $("#table_link ul li a").click(function(e){
    e.preventDefault();
    $("#main").load($(this).attr('href'));
    });
    
    document.getElementById('preach_link').click();
 });

     
  try {
   window.addEventListener('load', function(){
    setTimeout(scrollTo, 0, 0, 1);    
   }, false);
     
  } 
  catch(e) {}

  // ]]>
 </script>

 <link href="common.css?version=20190207_2" rel="stylesheet" type="text/css">
</head>
<body>
 <header>
  <div id="wrap_head">
   <h1 class="main_logo bold helv"><a href="index.jsp" id="logo">상계평강교회</a></h1>
   <a href="#" id="btn1"><span class="search_icon"></span></a>
   <div id="main_head_content">
    <form id="search" action="#" method="get">
     <fieldset>
      <legend>사이트 검색 입력</legend>
      <div class="bg_search">
       <div class="wrapBox">
	    <span class="search_left"></span>
        <input type="text" name="input_search" class="type_text" title="검색어 입력" maxlength="255"  />
       </div>
       <input type="submit" id="Btn" class="type_btn" title="검색" />
      </div>
     </fieldset>
    </form>
   </div>
  </div>
  <nav>
      <div id="table_link">
   <ul class="main_menu bold verd">
    <li><a href="./bulletin/list.jsp?tableName=preach_board"  title="preach" target="item_list" id="preach_link">주일설교</a></li>
    <li><a href="./bulletin/list.jsp?tableName=news_board" title="news"  target="item_list" >교회소식</a></li>
    <li><a href="./bulletin/list.jsp?tableName=bulletin_board" title="bulletin"  target="item_list" >게시판</a></li> 
    <li><a href="./bulletin/list.jsp?tableName=data_board" title="data" target="item_list" >자료실</a></li>
   </ul>
     </div>
  </nav>
 </header>
    <article>
        <div id="main">
        </div>
    </article>
     
 <footer>
  <div id="footer">
   <div class="foot_box">
    <ul>
     <li><a href="#" onclick="window.location.href='index.jsp'">상계평강교회 홈</a></li>    
    </ul>
    <input type="button" disabled value="로그인">
    <input type="button" value="PC버전" onclick="window.location.href='../index.jsp?move_pc_screen=1'">    
    <p class="privacy">
	 <a href="#">개인정보취급방침</a>
	 <span class="bar">|</span>
	 <a href="#">이용약관</a>
	</p>
   </div>
  </div>
  <div class="end_bar">
   <span class="copyrights helv">COPYRIGHT&copy; 2014 상계평강교회 ALL RIGHTS RESERVED.</span>
   <a href="javascript:window.scrollTo(0,0);"><img src="images/btn_bot_top.png"></a>
  </div>
 </footer>
</body>
</html>