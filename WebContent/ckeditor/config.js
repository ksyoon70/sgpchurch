/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.language = 'kr';

// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';
   	config.filebrowserBrowseUrl = '/sgpchurch/ckfinder/ckfinder.html';
   config.filebrowserImageBrowseUrl = '/sgpchurch/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/sgpchurch/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash';
	//font config
	config.font_defaultLabel = '��������';
	config.fontSize_defaultLabel = '14px';
    config.font_names = '��������/���� ����;����/����;��������/��������;��������/��������;Gungsuh/Gungsuh;Arial/Arial;Tahoma/Tahoma;Verdana/Verdana';

//config toolbar
  
 config.toolbar_WriteToolBar =
[
    	['Source'],
	['Cut','Copy','Paste','PasteText','PasteFromWord','-','SpellChecker', 'Scayt'],
	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],				
	'/',
	['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	['Link','Unlink','Anchor'],
	['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
	'/',
	['Styles','Format','Font','FontSize'],
	['TextColor','BGColor'],
	['Maximize','-','About']
];

config.toolbar = 'WriteToolBar';
config.contentsCss = ['/sgpchurch/css/ckstyles.css']


CKEDITOR.on( 'dialogDefinition', function( ev ) {
  var tab, field, dialogName = ev.data.name,
      dialogDefinition = ev.data.definition;
  

  if( dialogName == 'image' )
  {   
 	var infoTab = dialogDefinition.getContents( 'info' );
        txtWidth = infoTab.get( 'txtWidth' );
       	txtWidth['default'] = 550;		
  }
});

/*
CKEDITOR.on( 'instanceReady', function( ev )
    {
    var editor = ev.editor,
      dataProcessor = editor.dataProcessor,
      htmlFilter = dataProcessor && dataProcessor.htmlFilter;
      
    // Output self closing tags the HTML4 way, like <br>.
    dataProcessor.writer.selfClosingEnd = '>';

    // Output dimensions of images as width and height
    htmlFilter.addRules(
    {
      elements :
      {
        $ : function( element )
        {
          if ( element.name == 'img' )
          {
            var style = element.attributes.style;

            if ( style )
            {
              // Get the width from the style.
              var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec( style ),
                width = match && match[1];

              // Get the height from the style.
              match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec( style );
              var height = match && match[1];


              if ( height )
              {
                element.attributes.style = element.attributes.style.replace( /(?:^|\s)height\s*:\s*(\d+)px;?/i , '' );
                element.attributes.height = height;
              }

              if ( width )
              {
                element.attributes.style = element.attributes.style.replace( /(?:^|\s)width\s*:\s*(\d+)px;?/i , '' );
                
            if (width > 640)
               {   
                  var new_height = (((640*100)/width)/100);
                  element.attributes.width = 640;
                  element.attributes.height = (Math.ceil(height*new_height));}
            else 
               {element.attributes.width = width;}
              }


            }
          }


          if ( !element.attributes.style || !element.attributes.style.replace(/^\s*([\S\s]*?)\s*$/, '$1').length )
            delete element.attributes.style;

          return element;
        }
      }
    } );
    
    });
*/

};

