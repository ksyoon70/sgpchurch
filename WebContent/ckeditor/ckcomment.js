/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'kr';

// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	/*config.removeButtons = 'Underline,Subscript,Superscript';
   	config.filebrowserBrowseUrl = '/sgpchurch/ckfinder/ckfinder.html';
   config.filebrowserImageBrowseUrl = '/sgpchurch/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/sgpchurch/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash';*/
	
	
	config.filebrowserBrowseUrl = '/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = '/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash';
		
	//font config
	config.font_defaultLabel = '��������';
	config.fontSize_defaultLabel = '10px';
    config.font_names = '��������/���� ����;����/����;��������/��������;��������/��������;Gungsuh/Gungsuh;Arial/Arial;Tahoma/Tahoma;Verdana/Verdana';

//config toolbar
  
 config.toolbar_CommentToolBar =
[
	['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	['NumberedList','BulletedList'],
	['TextColor','BGColor'],
	['Maximize','-','About']
];

config.toolbar = 'CommentToolBar';
config.removePlugins='elementspath';config.resize_enabled=false;


};


