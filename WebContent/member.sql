create table member(
id varchar(36) not null primary key,
passwd varchar(36) not null,
name varchar(256) not null,
nickname varchar(256) not null,
email varchar(256),
blog varchar(256),
rank int not null,
reg_date datetime not null,
question varchar(256) not null,
passwdhint varchar(256) not null,
year varchar(4) not null,
month varchar(2) not null,
day varchar(2) not null,
duty varchar(256)
)ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
