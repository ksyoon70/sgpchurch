<!DOCTYPE html>
<%@ page  contentType="text/html; charset=euc-kr"%>
<%@ page import = "sgpchurch.file.FileHandleBean" %>
<%@ include file="/view/color.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<link href="head.css?version=20190201_1" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<!--<script type="text/javascript"src="http://connect.facebook.net/en_US/all.js"></script> -->
<script src="jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<head>
	<title>상계평강교회 홈페이지에 소신 것을 환영합니다.</title>
</head>
<script>
var fbuser_id = null;
var fbuser_name =  null;
var fbpicture_path = null;
var fbLink = null;

// Chrome 브라우저는 무슨 이유인지는 모르겠지만, javascript 보다 Jquery에서 안정적으로 동작한다.
$(document).ready(function() {
	//alert("test");

		$.ajaxSetup({ cache: true });
	  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	FB.Event.subscribe('auth.login', function(response) {
	  // add something when facebook user signed up
	  if (response.status === 'connected') {
				//when facebook user is connected
				FB.api('/me', 'GET',{fields: 'first_name, last_name,name, id, email, picture.width(150).height(150)'}, function(user) {
				fbuser_name = user.name;
				fbuser_id = user.id;
				fbLink = null;
				console.log(user);
				if(fbuser_id != null)
				{
					// if facebook user id is not null,  makes directory
					var url = '/head/fbloginPro.jsp?fbuser_id='
					url += fbuser_id
					location.href = url;
				}

			});
		}
	},{scope:'public_profile,email'});

	});
  });



	function expireSession()
	{
		//window.location = "index.html";
	}
	function goHome()
	{
		parent.location.href = "index.jsp";
	}

	function loginOFF(){
		location.href = "/head/loginForm.jsp";
	}

	function Join() {
		location.href = "/head/loginForm.jsp";
	}
	function check_submit() {

		var timer = setInterval('expireSession()', 2000);

		if(!login.user_id.value) {
			alert("아이디를 입력하여 주세요");
			login.user_id.focus();
			return false;
		}
		if(!login.password.value)  {
			alert("비밀번호를 입력하여 주세요");
			login.password.focus();
			return false;
		}

		return true;
	}
</script>

<body >
<table width="1000" border="0" cellpadding="0" cellspacing="0" class="menu" valign="top">
	<tr><td height=5></td></tr>
	<tr>
	<td  valign="center">

<table border="0" cellpadding="0" cellspacing="0" valign="center" height="21">
  	<tr>
<%
if(session.getAttribute("memId") == null)
{
%>
	<td class=top_menu  align=left>

	<!-- when mismatch aligment textbox and password use  style="vertical-align:middle"-->
	<form method=post name=login action=head/loginPro.jsp target="main" onsubmit="return check_submit();">
		<img src="logo.png" style="vertical-align:middle;">
		ID <input type="text"  style="vertical-align:middle" name="id" value="" size="12" maxlength="20"  align=absmiddle style="border:1px solid #cacaca;font-size:9pt;color:#252525;background-color:#ffffff;height:18px;padding:3px">
		PW <input type="password" style="vertical-align:middle" name="passwd" size="12" maxlength="20" align=absmiddle style="border:1px solid #cacaca;font-size:9pt;color:#252525;background-color:#ffffff;height:18px;padding:3px">
		<input type=image src=images/btn_login.gif border=0 align=absmiddle>
		<a href="head/inputForm.jsp" target="main"> <img src=images/btn_join.gif border=0 align=absmiddle width=53 height=17></a>

	</form>
	</td>

<%}else{%>
<td> <img src="logo.png" style="vertical-align:middle;"> </td>
<td align=left>
	<form  method="post" name=logout  action="head/logout.jsp">
		 <span style="font-size: 9pt">
	<%=session.getAttribute("memId")%>님이 <br>
	방문하셨습니다.
	</span>
     <input type="submit"  value="로그아웃" size="10" maxlength="15"  align=absmiddle style="border:1px solid #cacaca;font-size:9pt;color:#252525;background-color:#ffffff;height:18px;padding:3px">
     &nbsp;
     <input type="button" value="회원정보변경" size="10" maxlength="15"  align=absmiddle style="border:1px solid #cacaca;font-size:9pt;color:#252525;background-color:#ffffff;height:18px;padding:3px" onclick="javascript:parent.main.location.href = 'head/modifyForm.jsp'">
     </form>
</td>
<%}%>


	</tr>
	</table>
	</td>
	<td colspan=2 width="70" align=middle valign="left" class="top_menu" style="text-spacing:0px;">
	<a href="javascript:goHome()" ><img src="home-icon.gif" style="border:0" />
	<br>처음으로</a>
	</td>
	<td width="100" valign=right align=center><fb:login-button autologoutlink="true"></fb:login-button></td>
	</tr>
	<tr><td height=10></td></tr>
</table><!--- END TOP----------------------------------------->

<br>
<br>
<br>
<br>
<br>
<br>
<br>






</body>
</html>
