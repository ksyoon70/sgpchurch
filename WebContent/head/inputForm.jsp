<!DOCTYPE html>
<%@ page contentType="text/html; charset=euc-kr"%>
<%@ include file="/view/color.jsp"%>
<html>
<head>
<title>회원가입</title>
<link href="style.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

    
    function checkIt() {
        var userinput = eval("document.userinput");
        var index; 
        
        if(!userinput.id_valid.value.match("ok")) {
            alert("ID를 중복을 확인하세요!");
            return false;
        }
                         
       if(!userinput.id.value) {
            alert("ID를 입력하세요");
            return false;
        }
        
        if(!userinput.passwd.value ) {
            alert("비밀번호를 입력하세요");
            return false;
        }
        if(userinput.passwd.value != userinput.passwd2.value)
        {
            alert("비밀번호를 동일하게 입력하세요");
            return false;
        }
        if(!userinput.passwdhint.value) {
            alert("비밀번호 힌트 대답을 입력하세요.");
            return false;
        }
        if(!userinput.nickname.value) {
            alert("별명을 입력하세요.");
            return false;
        } 
       if(!userinput.name.value) {
            alert("사용자 이름을 입력하세요");
            return false;
        }
       index = userinput.year.options.selectedIndex;
       if(!userinput.year.options[index].text) {
           alert("태어난 년도를 확인하세요.");
           return false;
        }
        
       index = userinput.month.options.selectedIndex;
       if(!userinput.month.options[index].text) {
           alert("태어난 월을 확인하세요.");
           return false;
        }
       
       index = userinput.day.options.selectedIndex;
       if(!userinput.day.options[index].text) {
           alert("태어난 일을 확인하세요.");
           return false;
        }
       
       if(!userinput.email.value) {
           alert("E-mail을 입력하세요.");
           return false;
       }
            
    }

    // 아이디 중복 여부를 판단
    function openConfirmid(userinput) {
        // 아이디를 입력했는지 검사
        if (userinput.id.value == "") {
            alert("아이디를 입력하세요");
            return;
        }
        // url과 사용자 입력 id를 조합합니다.
        url = "confirmId.jsp?id=" + userinput.id.value ;
        
        // 새로운 윈도우를 엽니다.
        open(url, "confirm", 
        "toolbar=no, location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=300, height=200");
    }
    function join_cancel(){
    	parent.window.head.location.href = "../head.jsp";
		parent.main.location.href = "../main/main.jsp";
    }
</script>


<body bgcolor="<%=bodyback_c%>">

<form method="post" action="inputPro.jsp" name="userinput" onSubmit="return checkIt()">
	<input type="text" name="id_valid" size="10" maxlength="12">
  <table width="600" border="1" cellspacing="0" cellpadding="3" align="center" style="border-collapse:collapse" >
    <tr> 
    <td colspan="2" height="39" align="center" bgcolor="<%=light_grey%>">
       <font size="+1" ><b>회원가입</b></font></td>
    </tr>
    <tr> 
      <td width="200" bgcolor="<%=light_grey%>"><b>아이디 입력</b></td>
      <td width="400" bgcolor="<%=light_grey%>">&nbsp;</td>
    </tr>  

    <tr> 
      <td width="200"><font color="red">*</font> 사용자 ID </td>
      <td width="400"> 
        <input type="text" name="id" size="10" maxlength="12">
        <input type="button" name="confirm_id" value="ID중복확인" OnClick="openConfirmid(this.form)">
      </td>
    </tr>
    <tr> 
      <td width="200"><font color="red">*</font> 비밀번호 </td>
      <td width="400" > 
        <input type="password" name="passwd" size="15" maxlength="12">
      </td>
    <tr>  
      <td width="200"><font color="red">*</font> 비밀번호 확인</td>
      <td width="400"> 
        <input type="password" name="passwd2" size="15" maxlength="12">
      </td>
    </tr>
    <tr>  
      <td width="200"> 비밀번호 힌트 질문</td>
      <td width="400">        
        <select name="questions">
			<option value="졸업한 초등학교 이름은?">졸업한 초등학교 이름은?</option>
			<option value="태어난 장소?">태어난 장소?</option>
			<option value="어머니 성함은?">어머니 성함은?</option>
			<option value="아버지 성함은?">아버지 성함은?</option>
			<option value="가장 친한 친구이름?">가장 친한 친구이름?</option>
		</select>
      </td>
    </tr>
    <tr>  
      <td width="200"><font color="red">*</font> 비밀번호 힌트 질문 대답</td>
      <td width="400"> 
        <input type="text" name="passwdhint" size="60" maxlength="50">
      </td>
    </tr>
    <tr> 
      <td width="200"><font color="red">*</font> 별명</td>
      <td width="400"> 
        <input type="text" name="nickname" size="15" maxlength="10">
      </td>
    </tr>   
    <tr> 
      <td width="200" bgcolor="<%=light_grey%>"><b>개인정보 입력</b></td>
      <td width="400" bgcolor="<%=light_grey%>">&nbsp;</td>
    <tr>    
    <tr> 
      <td width="200"><font color="red">*</font> 사용자 이름</td>
      <td width="400"> 
        <input type="text" name="name" size="15" maxlength="10">
      </td>
    </tr>
    <tr> 
      <td width="200"><font color="red">*</font> 생년월일</td>
      <td width="400"> 
        <script language="javascript">
			<!--
			var today = new Date();
			var toyear = parseInt(today.getFullYear());
			//toyear = toyear + 1900;
			var start = toyear - 5;
			var end = toyear - 90;
			
			document.write("<font size=2><select name=year>");
			document.write("<option value='' selected>");
			for (i=start;i>=end;i--) document.write("<option>"+i);
			document.write("</select>년  ");
			
			document.write("<select name=month>");
			document.write("<option value='' selected>");
			for (i=1;i<=12;i++) document.write("<option>"+i);
			document.write("</select>월  ");
			
			document.write("<select name=day>");
			document.write("<option value='' selected>");
			for (i=1;i<=31;i++) document.write("<option>"+i);
			document.write("</select>일  </font>");
			//-->
			</script> 
      </td>
    </tr>
    <tr> 
      <td width="200"><font color="red">*</font> E-Mail</td>
      <td width="400"> 
        <input type="text" name="email" size="40" maxlength="30">
      </td>
    </tr>
    <tr> 
      <td width="200">직분</td>
      <td width="400"> 
        <select name="duty">
			<option value="없음">없음</option>
			<option value="평신도">평신도</option>
			<option value="집사">집사</option>
			<option value="권사">권사</option>
			<option value="장로">장로</option>
			<option value="전도사">전도사</option>
			<option value="목사">목사</option>
		</select>
      </td>
    </tr>   
    <tr> 
      <td width="200"> 홈페이지/블로그</td>
      <td width="400"> 
        <input type="text" name="blog" size="60" maxlength="50">
      </td>
    </tr>
    <tr> 
      <td colspan="2" align="left" bgcolor="<%=light_grey%>">
      		<font color="red">*</font>는 필수 입력 항목
      		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="submit" name="confirm" value="등   록" >
          <input type="reset" name="reset" value="다시입력">
          <input type="button" value="가입안함" onclick="join_cancel()">
      </td>
    </tr>
  </table>
</form>
</body>
</html>
