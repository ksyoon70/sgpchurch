<!DOCTYPE html>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import = "sgpchurch.logon.*" %>
<%@ include file="/view/color.jsp"%>

<html>
<head>
<title>회원정보수정</title>
<link href="style.css" rel="stylesheet" type="text/css">


<script language="JavaScript">
   <!-- 
    function checkIt() {
        var userinput = eval("document.userinput");
               
        if(!userinput.passwd.value ) {
            alert("비밀번호를 입력하세요");
            return false;
        }
        if(userinput.passwd.value != userinput.passwd2.value)
        {
            alert("비밀번호를 동일하게 입력하세요");
            return false;
        }       
      
       if(!userinput.passwdhint.value) {
            alert("비밀번호 힌트 대답을 입력하세요.");
            return false;
        }
       if(!userinput.nickname.value) {
           alert("별명을 입력하세요.");
           return false;
       } 
       if(!userinput.name.value) {
            alert("사용자 이름을 입력하세요");
            return false;
        }
       index = userinput.year.options.selectedIndex;
       if(!userinput.year.options[index].text) {
           alert("태어난 년도를 확인하세요.");
           return false;
        }
        
       index = userinput.month.options.selectedIndex;
       if(!userinput.month.options[index].text) {
           alert("태어난 월을 확인하세요.");
           return false;
        }
       
       index = userinput.day.options.selectedIndex;
       if(!userinput.day.options[index].text) {
           alert("태어난 일을 확인하세요.");
           return false;
        }
       
       if(!userinput.email.value) {
           alert("E-mail을 입력하세요.");
           return false;
       }
    }
-->
</script>

<%
    String id = (String)session.getAttribute("memId");
   
    LogonDBBean manager = LogonDBBean.getInstance();
    LogonDataBean c = manager.getMember(id);

	try{
%>

<body bgcolor="<%=bodyback_c%>">
<form method="post" action="modifyPro.jsp" name="userinput" onsubmit="return checkIt()">

  <table width="600" border="1" cellspacing="0" cellpadding="3"  align="center">
    <tr > 
      <td  colspan="2" height="39" bgcolor="<%=grey%>" align="center">
	     <font size="+1" ><b>회원 정보수정</b></font></td>
    </tr>
    <tr>
      <td colspan="2" class="normal" align="center">회원의 정보를 수정합니다.</td>
    </tr>
     <tr> 
      <td width="200" bgcolor="<%=light_grey%>"><b>아이디 입력</b></td>
      <td width="400" bgcolor="<%=light_grey%>">&nbsp;</td>
    <tr>  

    <tr> 
      <td  width="200"> 사용자 ID</td>
      <td  width="400"><%=c.getId()%></td>
    </tr>
    
     <tr> 
      <td width="200"><font color="red">*</font> 비밀번호</td>
      <td width="400"> 
        <input type="password" name="passwd" size="15" maxlength="12" >
      </td>
    <tr>
    <tr>  
      <td width="200"><font color="red">*</font> 비밀번호 확인</td>
      <td width="400"> 
        <input type="password" name="passwd2" size="15" maxlength="12">
      </td>
    </tr>  
    <tr>
    <tr>  
      <td width="200"><font color="red">*</font> 비밀번호 힌트 질문</td>
      <td width="400">        
        <select name="questions" >        
         	<% if(c.getQuestions() != null){
         	if(c.getQuestions().equals("졸업한 초등학교 이름은?")){ %>
        	<option value="졸업한 초등학교 이름은?" selected="selected">졸업한 초등학교 이름은?</option>
        	<%}
        	else
        	{%>
			<option value="졸업한 초등학교 이름은?">졸업한 초등학교 이름은?</option>
			<%}
			if(c.getQuestions().equals("태어난 장소?")){%>
			<option value="태어난 장소?" selected="selected">태어난 장소?</option>
			<%}
			else
			{%>
			<option value="태어난 장소?">태어난 장소?</option>
			<%}
			if(c.getQuestions().equals("어머니 성함은?")){%>
			<option value="어머니 성함은?" selected="selected">어머니 성함은?</option>
			<%}
			else{%>
			<option value="어머니 성함은?">어머니 성함은?</option>
			<%}
			if(c.getQuestions().equals("아버지 성함은?")){%>
			<option value="아버지 성함은?" selected="selected">아버지 성함은?</option>
			<%}
			else{%>
			<option value="아버지 성함은?">아버지 성함은?</option>
			<%}
			if(c.getQuestions().equals("가장 친한 친구이름?")){%>
			<option value="가장 친한 친구이름?" selected="selected">가장 친한 친구이름?</option>
			<%}
			else{%>
			<option value="가장 친한 친구이름?">가장 친한 친구이름?</option>
			<%} }%>
		</select>
      </td>
    </tr>
    <tr>  
      <td width="200"><font color="red">*</font> 비밀번호 힌트 질문 대답</td>
      <td width="400"> 
        <input type="text" name="passwdhint" size="60" maxlength="50" value = "<%=c.getPasswdhint()%>">
      </td>
    </tr>
    <tr> 
      <td width="200"><font color="red">*</font> 별명</td>
      <td width="400"> 
        <input type="text" name="nickname" size="15" maxlength="10" value = "<%=c.getNickname()%>">
      </td>
    </tr>   
    <tr> 
      <td  width="200" bgcolor="<%=light_grey%>"><b>개인정보 입력</b></td>
      <td width="400" bgcolor="<%=light_grey%>">&nbsp;</td>
    <tr>  
    <tr> 
      <td   width="200"><font color="red">*</font> 사용자 이름</td>
      <td  width="400"> 
        <input type="text" name="name" size="15" maxlength="20" value="<%=c.getName()%>">
      </td>
    </tr>
    <tr> 
      <td width="200"><font color="red">*</font> 생년월일</font></td>
      <td width="400"> 
        <script language="javascript">
			<!--
			var today = new Date();
			var toyear = parseInt(today.getFullYear());
			var start = toyear - 5;
			var end = toyear - 90;
			
			document.write("<font size=2><select name=year >");
			document.write("<option value='' selected>");
			for (i=start;i>=end;i--)
			{
				<%
					int year = Integer.parseInt(c.getYear());					
				%>
				if(i == <%=year%>)
					document.write("<option selected='selected'>"+i);
				else
					document.write("<option>"+i);
			}
			document.write("</select>년  ");
			
			document.write("<select name=month >");
			document.write("<option value='' selected>");
			for (i=1;i<=12;i++)
			{
				<%
				int month = Integer.parseInt(c.getMonth());					
				%>
				if(i == <%=month%>)
					document.write("<option selected='selected'>"+i);
				else
					document.write("<option>"+i);
			}			
			document.write("</select>월  ");
			
			document.write("<select name=day>");
			document.write("<option value='' selected>");
			for (i=1;i<=31;i++)
			{
				<%
				int day = Integer.parseInt(c.getDay());					
				%>
				if(i == <%=day%>)
					document.write("<option selected='selected'>"+i);
				document.write("<option>"+i);
			}
			document.write("</select>일  </font>");
			//-->
			</script> 
      </td>
    </tr>    
   <tr> 
      <td width="200"><font color="red">*</font> E-Mail</td>
      <td width="400">
	    <%if(c.getEmail()==null){%>
		  <input type="text" name="email" size="40" maxlength="30" >
		<%}else{%>
          <input type="text" name="email" size="40" maxlength="30" value="<%=c.getEmail()%>">	
		<%}%>
      </td>
    </tr>
    <tr> 
      <td width="200"> 직분</td>
      <td width="400"> 
        <select name="duty">
        	<% if(c.getDuty() != null){
         	if(c.getDuty().equals("없음")){ %>
        	<option value="없음" selected="selected">없음</option>
        	<%}	else{ %>
				<option value="없음">없음</option>
         	<%}
         	if(c.getDuty().equals("평신도")){ %>
         	<option value="평신도" selected="selected">평신도</option>
         	<%}else{ %>
			<option value="평신도">평신도</option>
         	<%}
         	if(c.getDuty().equals("집사")){ %>
         	<option value="집사" selected="selected">집사</option>
         	<%}else{ %>
			<option value="집사">집사</option>
         	<%}
         	if(c.getDuty().equals("권사")){ %>
         	<option value="권사" selected="selected">권사</option>
         	<%}else{ %>
			<option value="권사">권사</option>
         	<%}
         	if(c.getDuty().equals("장로")){ %>
         	<option value="장로" selected="selected">장로</option>
         	<%}else{ %>
			<option value="장로">장로</option>
         	<%}
         	if(c.getDuty().equals("전도사")){ %>
         	<option value="전도사" selected="selected">전도사</option>
         	<%}else{ %>
			<option value="전도사">전도사</option>
         	<%}
         	if(c.getDuty().equals("목사")){ %>
         	<option value="목사" selected="selected">목사</option>
         	<%}else{ %>
			<option value="목사">목사</option>
         	<%}}%>      
		</select>
      </td>
    </tr>
    <tr> 
      <td width="200"> Blog</td>
      <td width="400"> 
	    <%if(c.getBlog()==null){%>
		  <input type="text" name="blog" size="60" maxlength="50" >
		<%}else{%>
          <input type="text" name="blog" size="60" maxlength="50" value="<%=c.getBlog()%>">
		<%}%>
      </td>
    </tr>      
    <tr> 
      <td colspan="2" align="center" bgcolor="<%=light_grey%>"> 
       <input type="submit" name="modify" value="수   정" >
       <input type="button" value="취  소" onclick="javascript:window.location='main.jsp'">      
      </td>
    </tr>
  </table>
</form>
</body>
<%}catch(Exception e){}%>
</html>