<!DOCTYPE html>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import = "sgpchurch.logon.LogonDBBean" %>
<%@ page import = "sgpchurch.encrypt.LocalEncrypter" %>
<%@ page import = "java.sql.Timestamp" %>

<% request.setCharacterEncoding("euc-kr");%>

<jsp:useBean id="member" class="sgpchurch.logon.LogonDataBean">
    <jsp:setProperty name="member" property="*" />
</jsp:useBean>

<%
    member.setReg_date(new Timestamp(System.currentTimeMillis()) );
	 member.setRanking(0);
	 LocalEncrypter encrypt = new LocalEncrypter("MD5", member.getPasswd());
	 member.setPasswd(encrypt.getEncryptData());
    LogonDBBean manager = LogonDBBean.getInstance();
    manager.insertMember(member);

    response.sendRedirect("loginForm.jsp");
%>