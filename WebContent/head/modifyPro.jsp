<!DOCTYPE html>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import = "sgpchurch.logon.LogonDBBean" %>
<%@ page import = "sgpchurch.encrypt.LocalEncrypter" %>
<%@ include file="/view/color.jsp"%>

<% request.setCharacterEncoding("euc-kr");%>

<jsp:useBean id="member" class="sgpchurch.logon.LogonDataBean">
    <jsp:setProperty name="member" property="*" />
</jsp:useBean>
 
<%
    String id = (String)session.getAttribute("memId");
	member.setId(id);
	LocalEncrypter encrypt = new LocalEncrypter("MD5", member.getPasswd());
	member.setPasswd(encrypt.getEncryptData());
	LogonDBBean manager = LogonDBBean.getInstance();
    manager.updateMember(member);
 %>
<link href="style.css" rel="stylesheet" type="text/css">

<table width="270" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr bgcolor="<%=grey%>"> 
    <td height="39"  align="center">
	  <font size="+1" ><b>회원정보가 수정되었습니다.</b></font></td>
  </tr>
  <tr>
    <td bgcolor="<%=light_grey%>" align="center"> 
      <p>입력하신 내용대로 수정이 완료되었습니다.</p>
    </td>
  </tr>
  <tr>
    <td bgcolor="<%=light_grey%>" align="center"> 
      <form>
	    <input type="button" value="메인으로" onclick="window.location='../main/main.jsp'">
      </form>
      5초후에 메인으로 이동합니다.<meta http-equiv="Refresh" content="5;url=../main/main.jsp" >
    </td>
  </tr>
</table>
</body>
</html>
