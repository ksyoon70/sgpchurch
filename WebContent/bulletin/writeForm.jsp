<!DOCTYPE html>
<%@ page contentType="text/html; charset=euc-kr" %>
<%@ taglib uri="http://ckfinder.com" prefix="ckfinder"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>
<%@ page import = "sgpchurch.logon.*" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ include file="/view/color.jsp"%>
<% 
  int num=0,ref=1,re_step=0,re_level=0;
  String writer="",email="",passwd="";
  
  String id = (String)session.getAttribute("memId");
  String pageNum = "1";
  String tableName = request.getParameter("tableName");
  String idDir;
 
  LogonDBBean manager = LogonDBBean.getInstance();
  
  if(id != null)
  {
	LogonDataBean c = manager.getMember(id);    
	email = c.getEmail();
	writer = c.getName(); 
	passwd = c.getPasswd();	
	idDir = id;  
  }
  else
  {
	  // not login
	  idDir = request.getParameter("id");
  }
  
  try{  
    if(request.getParameter("num")!=null){
	num=Integer.parseInt(request.getParameter("num"));
	ref=Integer.parseInt(request.getParameter("ref"));
	re_step=Integer.parseInt(request.getParameter("re_step"));
	re_level=Integer.parseInt(request.getParameter("re_level"));	
	}
    
    Calendar mon = Calendar.getInstance();
    mon.add(Calendar.MONTH , 2);
    String monthsAfter = new java.text.SimpleDateFormat("yyyy-MM-dd").format(mon.getTime());
    	
%>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<!--link href 문이 script 문 위에 있어야 함-->
<link href="style.css?version=20140107_1" rel="stylesheet" type="text/css">
<link href="sns.css?version=20130906_1" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../ckfinder/ckfinder.js"></script>
<script language="JavaScript" src="script.js"></script>
<script src="../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<head>
		 <title>상계평강교회 소식</title> 
</head>
<script>
 
	// Here we run a very simple test of the Graph API after login is successful. 
	  // This testAPI() function is only called in those cases. 
	  /*function testAPI() {
	    console.log('Welcome!  Fetching your information.... ');
	    FB.api('/me', function(response) {
	      console.log('Good to see you, ' + response.name + '.');
	    });
		}*/
		
var fbuser_id = null;
var fbuser_name =  null;
var fbemail = null;
var fbaccessTocken = null;
var fbpicture_path = null;

$(document).ready(function() {
	//alert("readay");
		$.ajaxSetup({ cache: true });
	  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
		FB.init({
		  appId: '791440404206573',
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	// Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      fabceloginChk = true;    
      getFBProfile(); 
      
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.            
     // FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.          
      //FB.login();
    }
  });
	
  //fake();
	
	 function getFBProfile(){
	
			FB.api('/me', 'GET',{fields: 'first_name, last_name,name,email, id,picture.width(150).height(150)'}, function(user) {  
				fbuser_name = user.name;
				fbemail = user.email;
				fbuser_id = user.id;		
				var fbLink = null;
					
					fbpicture_path = user.picture.data.url;
					 document.getElementById('userPic').innerHTML = ( 
								'<a href="' +  fbLink + '"' + 'target="_blank"' + '>' + '<img src="' + fbpicture_path + '"' + 'width = 32px' + '"> ' + '</a>'  
							  + '<span class="fb_name">' + fbuser_name  + '</span>');
					/*document.getElementById('userName').innerHTML = (
					'<a href="' + fbLink + '"' + 'target="_blank"' + '>' + fbuser_name + '</a>'
					);*/
					
					document.getElementById('fbid').value= user.id;		// facebook user id set
					document.getElementById('fbname').value= fbuser_name;		// facebook user name set
					document.getElementById('fblink').value= fbLink;		// facebook user link set
					document.getElementById('fbpic').value= fbpicture_path;		// facebook user picture set
					document.getElementById('fbemail').value= fbemail;		// facebook user email set			
					if(!<%=id%>)
					{
						document.getElementById('comm_id').value= fbuser_id;		// facebook id set
						document.getElementById('comm_writer').value= fbuser_name;		// facebook writer set
					}		
				   
			});
	
		}
	
	});
  });

function fake(){
	
	fbuser_name = 'Kyoung Seop Yoon';
	fbemail = null;
	fbuser_id = '100001843662259';		
	var fbLink = 'https://www.facebook.com/kyoungseop.yoon';
	
	alert("infake");
			

			fbpicture_path = 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash2/t5/1118522_100001843662259_1684853605_n.jpg';
			document.getElementById('userPic').innerHTML = ( 
						'<a href="' +  fbLink + '"' + 'target="_blank"' + '>' + '<img src="' + fbpicture_path + '"' + 'width = 32px' + '"> ' + '</a>'  
					  + '<span class="fb_name">' + fbuser_name  + '</span>');
			/*document.getElementById('userName').innerHTML = (
			'<a href="' + fbLink + '"' + 'target="_blank"' + '>' + fbuser_name + '</a>'
			);*/
			document.getElementById('fbid').value= fbuser_id;		// facebook user id set
			document.getElementById('fbname').value= fbuser_name;		// facebook user name set
			document.getElementById('fblink').value= fbLink;		// facebook user link set
			document.getElementById('fbpic').value= fbpicture_path;		// facebook user picture set
			document.getElementById('fbemail').value= fbemail;		// facebook user email set			
			if(!<%=id%>)
			{
				document.getElementById('comm_id').value= fbuser_id;		// facebook id set
				document.getElementById('comm_writer').value= fbuser_name;		// facebook writer set
			}
}

function ckeckFaceBook(){
	
	 FB.getLoginStatus(function(response) {  // 페이스북 로그인 상태를 조회해서
		
		if (response.status === 'connected') {  // 접속되어 있다면
			//alert('페북 연결됨');
			fabceloginChk = true;
			getFBProfile();
						
		}
		else{
			//alert('페북 연결안됨');
			fabceloginChk = false;
		}		
		
	},true);		// true를 넣어주면 cashe를 쓰지 않음		
}  	 

$(function() {
	  $( "#datepicker1" ).datepicker({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    //changeMonth: true, // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
	    //changeYear: true, // 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    dayNames: ['일','월','화','수','목','금','토'],
	    dayNamesShort: ['일','월','화','수','목','금','토'],
	    dayNamesMin: ['일','월','화','수','목','금','토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년',
	    showButtonPanel: true, // 캘린더 하단에 버튼 패널을 표시한다. 
	    closeText: '닫기'  // 닫기 버튼 패널  
	    
	  });	  
	});
//reset writing
var SNS = {
		facebook: function(link, description)
		{			
			link = encodeURIComponent(link);
			//alert("link"+ link);
			description = encodeURIComponent(description);
			//alert("desc"+ description);
			var url = "http://www.facebook.com/sharer.php?u=" + link +  "&t=" + description;
			window.open(url,",");			
		}
}

function writeSave()
{
	return true;
}
function reset_writing()
{
	var content =  document.getElementById("subject");
	content.value="";
	CKEDITOR.instances.content.editable().setHtml('');
}

function reset_date()
{
	var date =  document.getElementById("datepicker1");
	date.value="영구";	
}

</script>
   
<body bgcolor="<%=bodyback_c%>">
</script>
<center><b>글쓰기</b></center>
<form method="post" name="writeform" action="writePro.jsp?tableName=<%=tableName%>" onsubmit="return writeSave()">
<input type="hidden" name="num" value="<%=num%>">
<input type="hidden" name="ref" value="<%=ref%>">
<input type="hidden" name="re_step" value="<%=re_step%>">
<input type="hidden" name="re_level" value="<%=re_level%>">
<input type="hidden" name="writer" id="comm_writer" value="<%=writer%>">
<input type="hidden" name="email"  value="<%=email%>">
<input type="hidden" name="passwd" value="1234">
<input type="hidden" name="id" id="comm_id" value="<%=id%>">  
<input type="hidden" name="fbuser_id" id="fbid" value="">
<input type="hidden" name="fbuser_name" id="fbname" value="">
<input type="hidden" name="fbuser_link" id="fblink" value="">
<input type="hidden" name="fbuser_pic" id="fbpic" value="">
<input type="hidden" name="fbuser_email" id="fbemail" value="">
<table frame="hsides" width="100%" border="1" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="border-collapse:collapse">
   <tr>
    <td style ="border-right: none;" bgcolor="<%=light_grey%>">
	    <div id="userPic">
	</td>
   <td align="right" style="border-left: none;" bgcolor="<%=light_grey%>">
	<a href="list.jsp?tableName=<%=tableName%>"> 글목록</a>    
   </td>
   </tr>
   <tr>
    <td  width="70"  bgcolor="<%=light_grey%>" align="center" >제 목</td>
    <td  width="330">    
       <input id="subject" class="basic_write_title" type="text"   maxlength="50" name="subject"></td>	
  </tr>
<tr>
    <td  width="70"  bgcolor="<%=light_grey%>" align="center" >게시종료일</td>
    <td  width="330" noWrap>    <!-- noWrap 줄바꿈 금지 --> 
         <input type="button" value="영구적용" OnClick="reset_date()"><input class="basic_datepicker" type="text" name="due_date" value="<%=monthsAfter%>" id="datepicker1">      
     </td>		
  </tr>    
  <tr> 
     <td  width="70"  bgcolor="<%=light_grey%>" align="center" >내 용</td>
    <td align="left">
    <textarea class="ckeditor" id="content" name="content"></textarea>
	<script type="text/javascript">
		var editor = CKEDITOR.replace('content',{
			//ckeditor에서 custom configuration 파일을 사용하려면 webroot까지 써줘야한다.
			customConfig: '/sgpchurch/ckeditor/ckwriter.js',
			width:'100%',
			height:300,
			filebrowserBrowseUrl : '/sgpchurch/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl : '/sgpchurch/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : '/sgpchurch/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : 
		 	   '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files&currentFolder=/<%=id%>/',
			filebrowserImageUploadUrl : 
			   '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images&currentFolder=/<%=id%>/',
			filebrowserFlashUploadUrl : '/sgpchurch/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash&currentFolder=/<%=id%>/',
		});	
	</script>     
     </td>
  </tr> 
<tr>      
 <td colspan=2 bgcolor="<%=light_grey%>" align="center"> 
  <input type="submit" value="등록" >  
  <!-- <input type="reset" value="다시작성"> -->
  <input type="button" value="다시작성" OnClick="reset_writing()">
  <!--  <input type="button" value="목록보기" OnClick="window.location='list.jsp'?tableName=<%=tableName%>">-->
  <input type="button" value="목록보기" 
       onclick="document.location.href='list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>'">
</td></tr>
<!--
<tr>
<td colspan="2"><input id="set_facebook" class="chk setInfo" type="checkbox"></input><img class="ic_fb" src='images/facebook.gif'>페이스북</td></tr> -->
</table>    
 <%
  }catch(Exception e){}
%>     
</form>      
</body>
</html>      

