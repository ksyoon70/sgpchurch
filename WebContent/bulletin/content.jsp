<!DOCTYPE html>

<%@ page contentType = "text/html; charset=euc-kr" %>
<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "sgpchurch.board.BoardDataBean" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "sgpchurch.logon.*" %>
<%@ page import = "java.sql.Timestamp" %>
<%@ include file="/view/color.jsp"%>
<%@ taglib uri="http://ckfinder.com" prefix="ckfinder"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>

<html lang="ko" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<link href="style.css?version=20140105_12" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../jquery/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" type="text/css" media="all" />
<!--<script type="text/javascript"src="http://connect.facebook.net/en_US/all.js"></script> -->
<script type="text/javascript" src="../ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script src="../jquery/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../jquery/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>




<head>
<title>게시판</title>
</head>
<%

   String writer="",email="",passwd="";
   String id = (String)session.getAttribute("memId");
   int num = Integer.parseInt(request.getParameter("num"));
   String pageNum = request.getParameter("pageNum");
   String currentId = (id != null) ? id  : request.getParameter("id");
   String tableName = request.getParameter("tableName");
   int co_count = 0;		//comment count
   

   SimpleDateFormat sdf = 
        new SimpleDateFormat("yyyy-MM-dd HH:mm");

   try{
      BoardDBBean dbPro = BoardDBBean.getInstance();
      BoardDataBean article =  dbPro.getArticle(tableName,num,currentId);
  
	  int ref=article.getRef();
	  int re_step=article.getRe_step();
	  int re_level=article.getRe_level();
	  String writer_id = article.getId();
	  boolean isWriter = (writer_id.equals(id)) ? true : false;	  
	  	  	  
	  if(id != null)
	  {
		  LogonDBBean manager = LogonDBBean.getInstance();
		  LogonDataBean c = manager.getMember(id);  
		  email = c.getEmail();
		  writer = c.getName(); 
		  passwd = c.getPasswd();
		  
	  }  
	  
	  //read comment articles
	  List coArticleList = null;
	  co_count = dbPro.getSubArticleCount(tableName,num);
	  if(co_count > 0)
	  {
		  coArticleList =  dbPro.coGetArticles(tableName,num);	  	  
	  }
	  
	  
	
%>
<script>

var fabceloginChk = false;
var fbuser_id = null;
var fbuser_name =  null;
var fbemail = null;
var fbaccessTocken = null;
var fbpicture_path = null;
  
  if(top == self)
	{
	
		var addr = self.location.href;
		var domain;
		var sub;
		var forward_addr;
		
		domain = document.location.href.split('?');

		sub = domain[0].split('/');
		if(sub.length > 2)
		{				
			forward_addr = "../index.jsp?";
			forward_addr += 'page' + '=' + sub[sub.length-2] + '/' + sub[sub.length-1] + '&' + domain[1];
			self.location.href = forward_addr;
		}		
			
	}
		  
/*
{
   "id": "100001843662259",
   "name": "\uc724\uacbd\uc12d",
   "first_name": "\uacbd\uc12d",
   "last_name": "\uc724",
   "link": "http://www.facebook.com/kyoungseop.yoon",
   "username": "kyoungseop.yoon",
   "gender": "male",
   "locale": "ko_KR"
}
*/	

var fileurl = "data/upload/files";

	   function showqt(movieurl, wSize, hSize) {
    
	  var detect = navigator.userAgent.toLowerCase();
	  if (detect.indexOf('msie') + 1) {
      //ie에서는 param이 object의 child로 들어가면 재생이 안된다.
	      var object = document.createElement("span");
	      var output = "";
		    
        output += '<object id="movie" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width= "' + wSize + '" height= "' + hSize + '">';
       	output += '<param name="src" value="' + movieurl + '" >';
		output += '<param name="autoplay" value="true">';
		output += '<param name="pluginspage" value="http://www.apple.com/quicktime/download/">';
		output += '<param name="controller" value="true">';

		object.innerHTML = output;

		return object; 
       
				
	   }
        else {
        var object = document.createElement("object");
		object.setAttribute('height',hSize);
		object.setAttribute('width',wSize);
		object.setAttribute('data',movieurl);
		object.setAttribute('type','video/quicktime');
		object.setAttribute('id','movie');	
						
		var param1 = document.createElement("param");		
		param1.setAttribute('value','http://www.apple.com/quicktime/download/');
		param1.setAttribute('name','pluginurl');
		object.appendChild(param1);
		var param2 = document.createElement("param");		
		param2.setAttribute('value',"true");
		param2.setAttribute('name','controller');
		object.appendChild(param2);
		var param3 = document.createElement("param");		
		param3.setAttribute('value',"true");
		param3.setAttribute('name','autoplay');
		object.appendChild(param3);
		return object;		
	  }


	}
// Chrome 브라우저는 무슨 이유인지는 모르겠지만, javascript 보다 Jquery에서 안정적으로 동작한다.	
$(document).ready(function() {
  $.ajaxSetup({ cache: true });
  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
  	 window.fbAsyncInit = function() {
  FB.init({
    appId      : '791440404206573',
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  }); 
  
	  

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=791440404206573";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      fabceloginChk = true;
      //alert("connected");
      getFBProfile();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.      
      //alert("not authorized");
     // FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.    
      //alert("unconnected");
      //FB.login();
    }
  });
  
  function getFBProfile(){
	
	FB.api('/me', 'GET',{fields: 'first_name, last_name,name,email, id,picture.width(150).height(150)'}, function(user) {
		fbuser_name = user.name;
		fbemail = user.email;
		fbuser_id = user.id;		
		var fbLink = null; //user.link;
			

			fbpicture_path = user.picture.data.url;
			 document.getElementById('userPic').innerHTML = ( 
						'<a href="' +  fbLink + '"' + 'target="_blank"' + '>' + '<img src="' + fbpicture_path + '"' + 'width = 58px' + '"> ' + '</a>'  
					  );
			document.getElementById('userName').innerHTML = (
			'<a href="' + fbLink + '"' + 'target="_blank"' + 'class="fb_name"' +'>' + fbuser_name + '</a>'
			);
			document.getElementById('fbid').value= user.id;		// facebook user id set
			document.getElementById('fbname').value= fbuser_name;		// facebook user name set
			document.getElementById('fblink').value= fbLink;		// facebook user link set
			document.getElementById('fbpic').value= fbpicture_path;		// facebook user picture set
			document.getElementById('fbemail').value= fbemail;		// facebook user email set
			if(!<%=id%>)
			{
				document.getElementById('comm_id').value= fbuser_id;		// facebook id set
				document.getElementById('comm_writer').value= fbuser_name;		// facebook writer set
			}
			
	
		   
	});
}
  
};
  });
  
  
  var elem = document.getElementsByTagName("a");		
			
			//facebook login check		
			//ckeckFaceBook();
			
			
			if(elem.length)
			{
				for(var i = 0; i < elem.length; i++)
				{
					if(elem[i].innerHTML.match(fileurl))
					{
						
						var oImg=document.createElement("img");
						oImg.setAttribute('src', '../images/filesave16.gif');
						var pattern = /(^.*\/)([^&#?]*)/;
						var m = decodeURIComponent(elem[i].innerHTML).match(pattern);
						var filename = m[2];
						elem[i].innerHTML = filename;
						elem[i].parentNode.insertBefore(oImg,elem[i]);
						if (filename.match(/\.(mp3|ogg|wma|wav)$/i))
						{
							var audio = document.createElement("audio");
							var br = document.createElement("br");
							audio.preload ="none";
							audio.controls="controls";
							audio.src = elem[i].href;
							elem[i].parentNode.insertBefore(br,oImg);
							elem[i].parentNode.insertBefore(audio,br);
						}
						else if (filename.match(/\.(mp4|ogv|webm)$/i))
						{
							var video = document.createElement("video");
							var br = document.createElement("br");
							video.preload ="none";
							video.controls="controls";
							video.src = elem[i].href;
							video.width="480";
							video.height="272";							
							//var ext = filename.substring(filename.lastIndexOf('.').filename.length).toLowerCase();
							//alert(ext);
							/*if(ext == 'mp4')    
							 type='video/mp4';
							else if(ext == 'mov')
							type='video/mov';
							else if(ext == 'ogv')
							type='video/ogv';
							else if(ext == 'webm')
							type='video/webm';	*/
							elem[i].parentNode.insertBefore(br,oImg);
							elem[i].parentNode.insertBefore(video,br);
						}
						else if (filename.match(/\.(mov)$/i))
						{
							var object = showqt(elem[i].href,480,272);
							var br = document.createElement("br");							
							elem[i].parentNode.insertBefore(br,oImg);
							elem[i].parentNode.insertBefore(object,br);
							
						}					
					}											
					
				}
			}
});


function update_article()
{
}
function check_login()
	{
		var id = null;
		<%
			id = (String)session.getAttribute("memId");		
		%>
		id = <%=id%>;
		
		if(id)
		{
			return true;			
		}
		else if(fbuser_id)
		{			
			return true;
		}		
		else
		{
			alert("댓글을 쓰시려면 홈페이지 로그인을 하거나 Facebook 계정으로 로그인을 하십시오");
			location.href = "../head/loginForm.jsp";
			return false;
		
		}
		return true;
	}
	
</script>


<body bgcolor="<%=bodyback_c%>"> 

<center><b>교회소식 글내용 보기</b></center>
<br>
<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="table-layout:fixed"> --%> 
<table frame="hsides" bordercolor= <%=light_grey%> width="100%" border="1" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="border-collapse:collapse"> 
  <tr height="30">
    <td align="left" bgcolor="<%=light_grey%>" >조회수: <%=article.getReadcount()%></td>        
  </tr>
  <tr height="30">
    <td align="left" >글제목: <%=article.getSubject()%></td>    
  </tr>
  <% 
    if(article.getFbuser_name() != null && article.getWriter().matches(article.getFbuser_name())) { %>
	<tr height="50">
    <td align="left" >
     <a href="<%=article.getFbuser_link()%>" target="_blank" ><img src="<%=article.getFbuser_pic()%>" width = "58px" class="comment_image"> </a>
     글쓴이: <span class="fb_name"><a href="<%=article.getFbuser_link()%>" target="_blank" ><%=article.getFbuser_name()%></a></span>, 날짜: <%= sdf.format(article.getReg_date())%>, 종료일:<%=article.getDue_date()%> </td>    
    </tr>
	<% }else{ %>	
    <tr height="30">
    <td align="left" >글쓴이: <%=article.getWriter()%>, 날짜: <%= sdf.format(article.getReg_date())%>, 종료일:<%=article.getDue_date()%> </td>    
    </tr>
    <%}%>
 
  <tr>
  <!-- 아래는 글자 그대로 표시 -->
   <td class="ckstyle"><%=article.getContent()%></td>
  </tr>
  <tr height="30">
  	 <% if(isWriter || (article.getFbuser_name() != null && article.getWriter().matches(article.getFbuser_name()))) {%>
  	    <td bgcolor="<%=light_grey%>" align="left" > 
	  <input type="button" value="글수정" id="modify" onclick="document.location.href='updateForm.jsp?num=<%=article.getNum()%>&pageNum=<%=pageNum%>&tableName=<%=tableName%>&id='+ fbuser_id"> 		 
	  &nbsp;&nbsp;
	  <input type="button" value="글삭제" id="delete" onclick="document.location.href='deleteForm.jsp?num=<%=article.getNum()%>&pageNum=<%=pageNum%>&passwd=<%=article.getPasswd()%>&tableName=<%=tableName%>'">
	   &nbsp;&nbsp;
       <input type="button" value="글목록" 
       onclick="document.location.href='list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>'">
    </td>
  	<%} 
  	else {%>      
    <td bgcolor="<%=light_grey%>" align="left" >    
    
	   &nbsp;&nbsp;
       <input type="button" value="글목록" 
       onclick="document.location.href='list.jsp?tableName=<%=tableName%>&pageNum=<%=pageNum%>'">       
    </td>
    <%} %>
  </tr>
</table>
<br>

<!-- comment table draw -->
<%
	if(co_count > 0)
	{
			for (int i = 0 ; i < coArticleList.size() ; i++) {
          		BoardDataBean coArticle = (BoardDataBean)coArticleList.get(i);
%>         
<table style="margin-bottom:5px;" cellpadding="0" cellspacing="0" height="1" width="100%">
<tbody><tr>
    <td style="line-height:0;"></td>
    <td style="border-top:1px solid #ddd;" height="1" width="100%"></td>
</tr>
</tbody></table>

<table cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
    <td></td>
    <td style="text-align:left;" valign="top">
		<%
		if(coArticle.getFbuser_id() == null){
		%>	
        <img src="../images/noname.gif" width = "58px" class="comment_image">
        <%}else{ %>
			<a href="<%=coArticle.getFbuser_link()%>" target="_blank" ><img src="<%=coArticle.getFbuser_pic()%>" width = "58px" class="comment_image"> </a>
        <%}%>
    </td>
    <td width="2px"><div style="width:2px;"></div></td>
    <td bgcolor="#dedede" width="2"><div style="width:2px;"></div></td>
    <td><div style="width:10px;"></div></td>

    <td valign="top" width="100%">
        <table cellpadding="0" cellspacing="0" height="28" width="100%" background="../images/co_title_bg.gif">
        <tbody><tr>
            <!-- 이름, 아이피 -->
            
				<%
				if(coArticle.getFbuser_id() == null){
				%>
				<td width = "60">	
				<span><%=coArticle.getWriter()%></span>
				</td>
				<%}else{ %>
				<td width = "200">	
				<span><a href="<%=coArticle.getFbuser_link()%>" target="_blank" class="fb_name"><%=coArticle.getFbuser_name()%></a></span>
				</td>
				<%}%>	                
            
            <td>                
                <span style="float:left;" ><%= sdf.format(coArticle.getReg_date())%></span>
            </td>
        </tr>
        </tbody>
        </table>
        <table  cellpadding="0" cellspacing="0" width="100%">
        <tbody><tr>
            <td valign="top">
                <!-- 코멘트 출력 -->
                <div class="ckstyle" id="view_1298660"><%=coArticle.getContent()%></div>
            </td>
        </tr>
        </tbody></table>
        <!-- <div style="text-align:right; padding-right:10px;">
            <span class="mw_basic_comment_reply"><a href="javascript:comment_box('1298660', 'c');">답글쓰기</a></span>        </div>  -->       
</tr>
<tr>
    <td colspan="4" height="10"></td>
</tr>
</tbody></table>
<%
			}
	}
%>    
<p/>
<table style="margin-bottom:5px;" cellpadding="0" cellspacing="0" height="1" width="87%">
<tbody><tr>
    <td style="line-height:0;"></td>
    <td style="border-top:1px solid #ddd;" height="1" width="100%"></td>
</tr>
</tbody></table>
 
<table cellpadding="0" cellspacing="0" width="90%">
<tbody>
	<tr>
    <td></td>
    <td style="text-align:left;" valign="top">
		<div id="userPic"></div>
        <!--<img src="../images/noname.gif" width = "58px" class="comment_image">-->
    </td>
    <td width="2px"><div style="width:2px;"></div></td>
    <td bgcolor="#dedede" width="2"><div style="width:2px;"></div></td>
    <td><div style="width:10px;"></div></td>
    <td valign="top" width="100%"><div id="userName"></div></td>       
</tr>
</tbody>
</table>

<form method="post" name="writeCommentForm" action="writeComPro.jsp?pageNum=<%=pageNum%>&tableName=<%=tableName%>" onclick="return check_login()">
<input type="hidden" name="num" value="<%=num%>">
<input type="hidden" name="ref" value="<%=ref%>">
<input type="hidden" name="re_step" value="<%=re_step%>">
<input type="hidden" name="re_level" value="<%=re_level%>">
<input type="hidden" name="writer" id="comm_writer" value="<%=writer%>">
<input type="hidden" name="email" value="1234@1234.com">
<input type="hidden" name="passwd" value="1234">
<input type="hidden" name="id" id="comm_id" value="<%=id%>">
<input type="hidden" name="subject" value=" ">
<input type="hidden" name="fbuser_id" id="fbid" value="">
<input type="hidden" name="fbuser_name" id="fbname" value="">
<input type="hidden" name="fbuser_link" id="fblink" value="">
<input type="hidden" name="fbuser_pic" id="fbpic" value="">
<input type="hidden" name="fbuser_email" id="fbemail" value="">
<table frame="hsides" bordercolor= <%=light_grey%> width="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="<%=bodyback_c%>" align="center" style="border-collapse:collapse"> 
  <tr height="30">
      <td align="left" width = '90%'>    
    <textarea class="ckeditor" id="content" name="content"></textarea>
	<script type="text/javascript">
	  
		var editor = CKEDITOR.replace('content',{
			//ckeditor에서 custom configuration 파일을 사용하려면 webroot까지 써줘야한다.
			customConfig: '/sgpchurch/ckeditor/ckcomment.js'			
		});
		editor.on('instanceReady', function(evt)
		{
			
			
			var focusCnt = 0;			
			//style="color:rgb(169,169,169)" 로 써야지 크롬가 IE가 인식한다.
			editor.setData('<p><span style="color:rgb(169,169,169)">여기에 댓글을 입력하세요.</span></p>');	
			editor.on('focus',function(evt){
						
					if(!focusCnt)
					{
							editor.setData("");
							//editor.preventDefault();
					}
					editor.focus();
				focusCnt++;				
			});
			
		});
		
	</script>     
     </td>
     <td valign="bottom" align="center"><button type="submit" style="border-style:none;background-color:#ffffff;"><img src="../images/comment1.jpg" ></button></td>   
  </tr>
  </table>
  </form>
<%
 }catch(Exception e){} 
 %>   
	      
</body>
</html>      
