<!DOCTYPE html>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import = "sgpchurch.board.BoardDBBean" %>
<%@ page import = "java.sql.Timestamp" %>


<% request.setCharacterEncoding("euc-kr");%>

<jsp:useBean id="article" scope="page" class="sgpchurch.board.BoardDataBean">
   <jsp:setProperty name="article" property="*"/>
</jsp:useBean>
 
<%
	String pageNum = request.getParameter("pageNum");
	String tableName = request.getParameter("tableName");
    article.setReg_date(new Timestamp(System.currentTimeMillis()) );
	article.setIp(request.getRemoteAddr());
	

    BoardDBBean dbPro = BoardDBBean.getInstance();
    dbPro.insertCommentArticle(tableName,article);
    response.sendRedirect("content.jsp?num="+article.getNum()+"&pageNum="+pageNum+"&tableName="+tableName);
%>
