create table board(
num int not null primary key auto_increment,
writer varchar(10) not null,
email varchar(30),
subject varchar(50) not null,
passwd varchar(12) not null,
reg_date datetime not null,
readcount int default 0,
ref int not null,
re_step smallint not null,
re_level smallint not null,
content text not null,
ip varchar(20) not null
);

create table member(
id varchar(36) not null primary key,
passwd varchar(36) not null,
name varchar(256) not null,
nickname varchar(256) not null,
email varchar(256),
blog varchar(256),
reg_date datetime not null
);
